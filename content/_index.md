---
title: "GeoSpatial Solutions"
page: true

---

{{< rawhtml >}}
	<div class= "clearfix::before" style="background-image:url(/img/logo_unamed.svg);opacity: 0.1;position: absolute;top: 0px;right: 0px;bottom: 0px;left: 10%;width: 40%;height:100%"></div>
{{< /rawhtml >}}


## Drone Data acquisition

> Infrared, thermal and high resolution visible cameras
>> Seedlings countings, Crop health.\
>> Solar pannel checks\
>> Buildings watch


## Image/Data analysis

> You need rigorous statistics to be done?\
> Get the most out of your data with IA
>> Classification\
>> Image segmentation\
>> Change detection\
>> Timeseries Analysis\
>> ..

## Organize, Centralize and serve your Datas with PostgreSQL and PostGIS

> We install the most advanced OpenSource Database\
> We make it work 100%\
> We can build your data structure\
> You need custom functions? We will do it for you.

## Training sessions: become autonomous!
> SQL: Manage your data from their acquisition, to the answers they can bring you\
> Python: Make your computer do what you want\
> WebMapping: Offer your customers the maps they want


