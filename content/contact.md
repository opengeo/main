---
title: "Contact"
page: true
---
{{< rawhtml >}}
	<div class= "clearfix::before" style="background-image:url(/img/logo_unamed.svg);opacity: 0.1;position: absolute;top: 0px;right: 0px;bottom: 0px;left: 10%;width: 40%;height:100%"></div>
{{< /rawhtml >}}

**ADDRESS:**  
>>**OpenGeo**  
>>Parladère  
>>32420 Pellefigue

I am also in Dublin, please contact me: <contact@opengeo.fr>

**Tel:**  

>>France: +33 61 56 86 67  
>>Irland: +353 858 402 666
