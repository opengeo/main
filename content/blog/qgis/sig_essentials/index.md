---
author: David COLLADO
page: true
date: 2023-03-03
math: true
title: 'SIG: Fonctionnalités essentielles'
summary: Avant de démarrer une cession d'intro. à la télédétection, un petit récap. des outils SIG à maîtriser sur le bout des doigts (avec #qgis). Les anciens étudiants #ggat reconnaitront les toys data d'antan ;-)
categories: QGIS
---
Présentation des données simplifiées
====================================

Les données

-   Système de projection: EPSG: 27573

-   Format shp d'ESRI: glisser-coller le fichier portant cette extension

    -   villes, routes, lac, forets

-   Géométries au format texte (csv): Menu Couche $\rightarrow$ Ajouter
    $\rightarrow$Import texte délimité\

    ![image](./images/import_csv.png)

    -   PALOMB

-   Table non géométrique

    -   infogers

$\rightarrow$ **Les attributs sont aussi fondamentaux que les
géométries**

![image](./images/auch_env.png)

Sélections
==========

Attributaire
------------

La sélection attributaire

-   Sélection attributaire: Au sein de la table attributaire. Expression
    plus ou moins complexe

-   $\rightarrow$ Routes commençant par 'N'

    ![image](./images/selection_attrib_ex.png) 

Par localisation
----------------

La sélection par localisation

-   **Attention: même SRID**

-   Sélection par localisation: Relativement aux entités d'une autre
    couche

-   $\rightarrow$ Forêts traversées par une nationale

    ![image](./images/select_by_loc.png) 

Géotraitement
=============

Les géotraitements

A une couche
------------

Une seule couche $\rightarrow$ **Tjrs se demander: quid des attributs
obtenus??**

-   Tampon: Points de la carte à une distance bornée des entités
    $\rightarrow$ couche polygonale

-   $\rightarrow$ Points à moins de 100m d'une nationale

    ![image](buffer.png) 

-   Centroids: Centre de gravité (pixels, polygones)

-   Enveloppe convexe: Retire les "coins rentrants"

-   ..

A plusieurs couches
-------------------

Deux couches $\rightarrow$ **Tjrs se demander: quid des attributs
obtenus??**

-   Intersection

-   $\rightarrow$ Zones de forêts proche de palombières

    ![image](./images/intersection_geom_table.png) 

-   Union

-   $\rightarrow$ ..Et celles qui en sont loin grâce à une union et une
    selection. Il existe aussi l'outil "Différence"\...

    ![image](./images/union_geom_attrib.png) 

-   etc..

Les jointures
=============

Les jointures

Jointure attributaire
---------------------

Les jointures attributaires

-   Correspondance entre deux tables via une colonne commune

    ![image](./images/csv_ou_xls_info.png) 

    Fusion dans la Table attributaire $\Longrightarrow$

    ![image](./images/ta_villes.png) 

![image](./images/jointure_attrib.png) 

Résultat $\Longrightarrow$

![image](./images/jointure_resultat.png) 

Jointure spatiale
-----------------

La jointure spatiale $\rightarrow$ **Un des outils les plus fondamentaux
des SIG**

-   Correspondance de deux couches par coïncidence spatiale

-   **Attention: même SRID**

-   "Joindre les attributs par localisation"

    ![image](./images/ta_forets.png) 

    +

    ![image](./images/ta_plmb.png) 

"un à plusieurs: "

![image](./images/joint_spa_un_plus.png) 

Résultat $\Longrightarrow$

![image](./images/joint_spa_un_plus_res.png) 

ou si "un-à-un":

![image](./images/joint_spa_res_un_un.png) 

Merci de votre attention!


