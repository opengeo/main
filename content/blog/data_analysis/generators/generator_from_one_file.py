import numpy as np
import tensorflow as tf
import keras
import pandas as pd
import random

class DataGenerator(tf.keras.utils.Sequence):
   
    'Generates data for Keras'
    def __init__(self, ids_file,data_file,batch_size=256):
        ids_file=pd.read_csv(ids_file)
       
        data_file=pd.read_csv(data_file)
        data_file=data_file[['indf','dnbr','ndvi']]
        self.datas=data_file[(data_file['indf'].isin(ids_file.indf))]
        self.datas=self.datas[['ndvi','dnbr']].to_numpy()
        self.data_count=len(self.datas)
        self.batch_size=batch_size
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(self.data_count / self.batch_size))
      

    def __getitem__(self, idx):
        'Generate one batch of data'
        X, y = self.datas[idx*self.batch_size:(idx+1)*self.batch_size,0], self.datas[idx*self.batch_size:(idx+1)*self.batch_size,1]
        return X.astype('float32'), y.astype('float32')
    
    def get_infos(self):
        return self.data_count
    
    def on_epoch_end(self):
        'Updates indexes after each epoch'
       
        np.random.shuffle(self.datas)
       
   
