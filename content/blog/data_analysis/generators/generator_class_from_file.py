import numpy as np
import tensorflow as tf
import keras
import random
import pandas as pd


#We create the following class so that files get read only once and not once by generator instance
class DataSet():
    def __init__(self,ffid, radiom_file,dnbr_file, bands_used):
        self.df_dnbr=pd.read_csv(dnbr_file)
        self.df_dnbr=self.df_dnbr[(self.df_dnbr.ffid==ffid)]
        print(f"In fire {ffid}, We get "+str(len(self.df_dnbr))+" dnbr pixels")
        self.df_radiom=pd.read_csv(radiom_file)
        print("Radiometric file contains "+str(len(self.df_radiom))+" lines")
        #Filter radiom file dor dnbr's inds
        self.df_radiom=self.df_radiom[self.df_radiom['ind'].isin(self.df_dnbr.ind)]
        self.inds_count=len(self.df_radiom['ind'].unique())
        print("Fire file filtered by dnbr's ind for a resulting amount of "+str(self.inds_count)+" pixels")
        #file_bands available in file
        file_bands=self.df_radiom['band'].unique()
        if not all(band in file_bands for band in bands_used):
            print("Band(s) to process lacking from source file")
            exit()
        ###################################################################
        #                Samples creation
        ###################################################################       
        self.training_count=int(self.inds_count/2)
        self.test_count=int(self.inds_count/4)
        self.validation_count=self.inds_count-self.training_count-self.test_count
        idx=list(range(self.inds_count))
        random.shuffle(idx)
        self.training_idx=idx[0:self.training_count-1]
        self.test_idx=idx[self.training_count:self.training_count+self.test_count-1]
        self.validation_idx=idx[self.training_count+self.test_count:self.inds_count-1]
       

  

class DataGenerator(tf.keras.utils.Sequence):
    ds=None
    'Generates data for Keras'
    def __init__(self, ffid,radiom_file,dnbr_file, bands_enum,bands_used,sequence_length,data_type,batch_size=256):
        'Initialization'
        if DataGenerator.ds is None:
            DataGenerator.ds = DataSet(ffid, radiom_file,dnbr_file, bands_used)
        self.bands_enum=bands_enum
        self.sequence_length=sequence_length
        self.lines_per_band=DataGenerator.ds.inds_count*sequence_length
        self.batch_size = batch_size
        self.data_type=data_type
        self.bands_used=bands_used
        self.training_count=DataGenerator.ds.training_count
        self.test_count=DataGenerator.ds.test_count
        self.validation_count=DataGenerator.ds.validation_count
        self.training_idx=DataGenerator.ds.training_idx
        self.test_idx=DataGenerator.ds.test_idx
        self.validation_idx=DataGenerator.ds.validation_idx
        if self.data_type=="training":
            self.index = self.training_idx
        if self.data_type=="test":
            self.index = self.test_idx  
        if self.data_type=="validation":
            self.index = self.validation_idx
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.index) / self.batch_size))

    def __getitem__(self, idx):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.index[idx*self.batch_size:(idx+1)*self.batch_size]
           # Generate data
        X, y = self.__data_generation(indexes)

        return X, y
    
    def get_infos(self):
        return self.training_count, self.validation_count,self.test_count
    
    def on_epoch_end(self):
        'Updates indexes after each epoch'
        random.shuffle(self.index)
   
    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X=np.zeros((self.batch_size,self.sequence_length,len(self.bands_used)))
        y=np.zeros(self.batch_size)
        # Generate data
        for j, ID in enumerate(list_IDs_temp):
                y[j]=DataGenerator.ds.df_dnbr.loc[(DataGenerator.ds.df_dnbr['ind']==DataGenerator.ds.df_radiom.iloc[ID*self.sequence_length,0]),'dnbr']
                for i,band in enumerate(self.bands_used):
                   # Store sample
                   X[j][:,i]=DataGenerator.ds.df_radiom.iloc[self.bands_enum[band]*self.lines_per_band+ID*self.sequence_length:self.bands_enum[band]*self.lines_per_band+(ID+1)*self.sequence_length,2]

        return X, y
