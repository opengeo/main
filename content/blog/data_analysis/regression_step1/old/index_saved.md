---
title: " Basic regression with keras"
page: true
date: 2022-04-15
weight: 1
math: true
categories: -remote sensing
summary: Simple regression vs simplest NN between simulated NDVI and  DNBR spectral indices variables. The second being actually a llinear transform of the former with a small gaussian noise added. The purpose being to test the algorithm before applying it to real remote sensed datasets.
---



# Linear regression with keras with simulated datas

In this serie we will be dealing with a simple csv data file which structure is:


|indf|ndvi|dnbr|
|----|---|---|
||||

Basically, these datas are simulated random ndvi and $dnbr=3*ndvi+1+\epsilon$
with $\epsilon\sim \mathcal{N}(0,0.1)$ and $ ndvi\sim \mathcal{U}[0,1]$

The few next lines are just a tip for final layout centering.


```python
from IPython.core.display import HTML as Center

Center(""" <style>
.output_png {
    display: table-cell;indf
    text-align: center;
    vertical-align: middle;
    height:1600;
    width:600;
    
}
</style> """)

```




 <style>
.output_png {
    display: table-cell;indf
    text-align: center;
    vertical-align: middle;
    height:1600;
    width:600;

}
</style> 



Let's begin by charging the necessary libraries:


```python
import os
import re
import random
from matplotlib import pyplot as plt
from numpy.polynomial.polynomial import polyfit
from scipy.stats import gaussian_kde
import pandas as pd
import numpy as np
from tensorflow import keras
import keras.backend as K
from tensorflow.keras import layers
from generator_from_one_file import DataGenerator
```

Then we can prepare the datas


```python
data_file = "test_data.csv"
training_ids_file="training.csv"
#training_ids_file=data_path+"train.csv"
test_ids_file="test.csv"
#test_ids_file=data_path+"extra.csv"
validation_ids_file="validation.csv"
#validation_ids_file=data_path+"val.csv"
```

Doing a large usage of generators (one can refer to generator_from_one_file.py):


```python
batch_size=128
training=DataGenerator(training_ids_file,data_file,batch_size)
training_count=training.get_infos()
test=DataGenerator(test_ids_file,data_file,batch_size)
test_count=training.get_infos()
validation=DataGenerator(validation_ids_file,data_file,batch_size)
validation_count=validation.get_infos()
```

Time now to implement the network


```python
def build_model():
    model = keras.Sequential([
        layers.Input(shape=(1,)),
        layers.Dense(1,activation="linear"),
        
    ])
    opt = keras.optimizers.Adam()
    model.compile(optimizer=opt, loss="mse", metrics=["mae"])
    return model
```


```python

```


```python
class GetWeights(keras.callbacks.Callback):
  def __init__(self):
    super(GetWeights, self).__init__()
    self.weight_dict = {}
  def on_epoch_end(self, epoch, logs=None):
    w = model.layers[0].get_weights()[0]
    b = model.layers[0].get_weights()[1]
    if epoch == 0:
          # create array to hold weights and biases
        self.weight_dict['w_'] = w
        self.weight_dict['b_'] = b
    else:
          # append new weights to previously-created weights array
          self.weight_dict['w_'] = np.dstack(
              (self.weight_dict['w_'], w))
          # append new weights to previously-created weights array
          self.weight_dict['b_'] = np.dstack(
              (self.weight_dict['b_'], b))

```


```python
gw = GetWeights()
```

Let's define the Callback function that will allow us to analyse our model


```python
callbacks = [
    keras.callbacks.ModelCheckpoint('one_file.keras',
                                    save_best_only=True),
    keras.callbacks.TensorBoard(log_dir="./logs",histogram_freq=1),
    gw
]
```

Build and learn:


```python
model=build_model()
model.summary()
history = model.fit(training,
                    batch_size=batch_size,
                    steps_per_epoch=int(training_count/batch_size),
                    epochs=1000,
                    validation_data=validation,
                    validation_steps=int(validation_count/batch_size),
                    callbacks=callbacks,
                    use_multiprocessing=True,
                    workers=30,
                    )

```

    2022-04-22 00:04:08.981059: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
    To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
    2022-04-22 00:04:10.055655: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 4369 MB memory:  -> device: 0, name: Quadro P2000, pci bus id: 0000:3b:00.0, compute capability: 6.1
    2022-04-22 00:04:10.056393: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:1 with 4369 MB memory:  -> device: 1, name: Quadro P2000, pci bus id: 0000:af:00.0, compute capability: 6.1


    Model: "sequential"
    _________________________________________________________________
     Layer (type)                Output Shape              Param #   
    =================================================================
     dense (Dense)               (None, 1)                 2         
                                                                     
    =================================================================
    Total params: 2
    Trainable params: 2
    Non-trainable params: 0
    _________________________________________________________________
    Epoch 1/1000
     1/26 [>.............................] - ETA: 38s - loss: 21.3613 - mae: 3.8376WARNING:tensorflow:Callback method `on_train_batch_end` is slow compared to the batch time (batch time: 0.0028s vs `on_train_batch_end` time: 0.0037s). Check your callbacks.
    26/26 [==============================] - 4s 96ms/step - loss: 24.7462 - mae: 4.0529 - val_loss: 21.3078 - val_mae: 3.6848
    Epoch 2/1000
    26/26 [==============================] - 3s 96ms/step - loss: 18.2731 - mae: 3.3677 - val_loss: 19.5558 - val_mae: 3.5160
    Epoch 3/1000
    26/26 [==============================] - 4s 112ms/step - loss: 22.3376 - mae: 3.7837 - val_loss: 17.6091 - val_mae: 3.2963
    Epoch 4/1000
    26/26 [==============================] - 3s 98ms/step - loss: 18.7230 - mae: 3.4175 - val_loss: 19.0059 - val_mae: 3.4903
    Epoch 5/1000
    26/26 [==============================] - 3s 97ms/step - loss: 17.4817 - mae: 3.2939 - val_loss: 20.1266 - val_mae: 3.5792
    Epoch 6/1000
    26/26 [==============================] - 3s 92ms/step - loss: 17.3418 - mae: 3.3203 - val_loss: 21.2026 - val_mae: 3.7290
    Epoch 7/1000
    26/26 [==============================] - 3s 99ms/step - loss: 17.3539 - mae: 3.3032 - val_loss: 21.5181 - val_mae: 3.7531
    Epoch 8/1000
    26/26 [==============================] - 4s 118ms/step - loss: 15.0335 - mae: 3.0693 - val_loss: 16.0682 - val_mae: 3.1307
    Epoch 9/1000
    26/26 [==============================] - 5s 152ms/step - loss: 17.8727 - mae: 3.3896 - val_loss: 18.8911 - val_mae: 3.4800
    Epoch 10/1000
    26/26 [==============================] - 4s 113ms/step - loss: 15.4921 - mae: 3.0679 - val_loss: 17.0490 - val_mae: 3.2154
    Epoch 11/1000
    26/26 [==============================] - 3s 100ms/step - loss: 16.3083 - mae: 3.1610 - val_loss: 13.8854 - val_mae: 2.8982
    Epoch 12/1000
    26/26 [==============================] - 3s 99ms/step - loss: 15.1345 - mae: 3.0026 - val_loss: 14.9437 - val_mae: 3.0418
    Epoch 13/1000
    26/26 [==============================] - 3s 100ms/step - loss: 14.0502 - mae: 2.8799 - val_loss: 14.6637 - val_mae: 2.9981
    Epoch 14/1000
    26/26 [==============================] - 5s 150ms/step - loss: 16.4915 - mae: 3.2121 - val_loss: 15.3291 - val_mae: 3.0421
    Epoch 15/1000
    26/26 [==============================] - 4s 120ms/step - loss: 15.3414 - mae: 3.0496 - val_loss: 12.7589 - val_mae: 2.7419
    Epoch 16/1000
    26/26 [==============================] - 3s 95ms/step - loss: 11.8523 - mae: 2.6623 - val_loss: 13.9220 - val_mae: 2.8595
    Epoch 17/1000
    26/26 [==============================] - 3s 99ms/step - loss: 12.3613 - mae: 2.6834 - val_loss: 15.1335 - val_mae: 3.0135
    Epoch 18/1000
    26/26 [==============================] - 4s 107ms/step - loss: 13.9285 - mae: 2.8406 - val_loss: 15.0159 - val_mae: 3.0008
    Epoch 19/1000
    26/26 [==============================] - 3s 96ms/step - loss: 15.0461 - mae: 3.0229 - val_loss: 12.8406 - val_mae: 2.7338
    Epoch 20/1000
    26/26 [==============================] - 3s 92ms/step - loss: 10.2771 - mae: 2.4446 - val_loss: 13.0618 - val_mae: 2.8045
    Epoch 21/1000
    26/26 [==============================] - 5s 148ms/step - loss: 12.3512 - mae: 2.6752 - val_loss: 10.8743 - val_mae: 2.5034
    Epoch 22/1000
    26/26 [==============================] - 4s 118ms/step - loss: 13.3836 - mae: 2.8198 - val_loss: 10.8719 - val_mae: 2.5024
    Epoch 23/1000
    26/26 [==============================] - 3s 100ms/step - loss: 11.3215 - mae: 2.5306 - val_loss: 10.5427 - val_mae: 2.4536
    Epoch 24/1000
    26/26 [==============================] - 3s 98ms/step - loss: 12.1785 - mae: 2.6589 - val_loss: 9.4334 - val_mae: 2.2908
    Epoch 25/1000
    26/26 [==============================] - 3s 97ms/step - loss: 12.7608 - mae: 2.7171 - val_loss: 11.0559 - val_mae: 2.5170
    Epoch 26/1000
    26/26 [==============================] - 4s 112ms/step - loss: 10.7941 - mae: 2.4670 - val_loss: 8.9168 - val_mae: 2.2329
    Epoch 27/1000
    26/26 [==============================] - 4s 138ms/step - loss: 9.4657 - mae: 2.2741 - val_loss: 10.9497 - val_mae: 2.4934
    Epoch 28/1000
    26/26 [==============================] - 3s 98ms/step - loss: 10.1921 - mae: 2.3913 - val_loss: 9.7550 - val_mae: 2.3336
    Epoch 29/1000
    26/26 [==============================] - 4s 137ms/step - loss: 8.5861 - mae: 2.1356 - val_loss: 9.5229 - val_mae: 2.2825
    Epoch 30/1000
    26/26 [==============================] - 3s 97ms/step - loss: 9.3914 - mae: 2.2462 - val_loss: 8.0924 - val_mae: 2.0951
    Epoch 31/1000
    26/26 [==============================] - 5s 154ms/step - loss: 7.8663 - mae: 2.0662 - val_loss: 8.5939 - val_mae: 2.1678
    Epoch 32/1000
    26/26 [==============================] - 3s 95ms/step - loss: 7.7516 - mae: 2.0539 - val_loss: 8.3261 - val_mae: 2.1314
    Epoch 33/1000
    26/26 [==============================] - 3s 98ms/step - loss: 9.9991 - mae: 2.3447 - val_loss: 7.5249 - val_mae: 2.0024
    Epoch 34/1000
    26/26 [==============================] - 3s 98ms/step - loss: 6.9616 - mae: 1.9261 - val_loss: 8.0986 - val_mae: 2.1088
    Epoch 35/1000
    26/26 [==============================] - 3s 100ms/step - loss: 7.7729 - mae: 2.0586 - val_loss: 7.2831 - val_mae: 1.9856
    Epoch 36/1000
    26/26 [==============================] - 3s 101ms/step - loss: 7.7261 - mae: 2.0203 - val_loss: 6.5038 - val_mae: 1.8661
    Epoch 37/1000
    26/26 [==============================] - 3s 99ms/step - loss: 8.3966 - mae: 2.1350 - val_loss: 7.8951 - val_mae: 2.0647
    Epoch 38/1000
    26/26 [==============================] - 4s 105ms/step - loss: 6.4243 - mae: 1.8469 - val_loss: 7.7253 - val_mae: 2.0327
    Epoch 39/1000
    26/26 [==============================] - 3s 102ms/step - loss: 7.3708 - mae: 1.9860 - val_loss: 8.0028 - val_mae: 2.0767
    Epoch 40/1000
    26/26 [==============================] - 3s 102ms/step - loss: 6.8862 - mae: 1.9189 - val_loss: 8.8368 - val_mae: 2.1815
    Epoch 41/1000
    26/26 [==============================] - 3s 98ms/step - loss: 6.0276 - mae: 1.7824 - val_loss: 7.4619 - val_mae: 1.9943
    Epoch 42/1000
    26/26 [==============================] - 4s 102ms/step - loss: 5.5316 - mae: 1.7188 - val_loss: 5.8255 - val_mae: 1.7566
    Epoch 43/1000
    26/26 [==============================] - 4s 104ms/step - loss: 6.1802 - mae: 1.8162 - val_loss: 5.1685 - val_mae: 1.6650
    Epoch 44/1000
    26/26 [==============================] - 3s 100ms/step - loss: 5.3425 - mae: 1.6739 - val_loss: 6.0007 - val_mae: 1.7881
    Epoch 45/1000
    26/26 [==============================] - 4s 102ms/step - loss: 5.8331 - mae: 1.7541 - val_loss: 5.5013 - val_mae: 1.7091
    Epoch 46/1000
    26/26 [==============================] - 3s 101ms/step - loss: 5.3210 - mae: 1.6900 - val_loss: 6.3879 - val_mae: 1.8419
    Epoch 47/1000
    26/26 [==============================] - 4s 101ms/step - loss: 5.8794 - mae: 1.7741 - val_loss: 5.8275 - val_mae: 1.7597
    Epoch 48/1000
    26/26 [==============================] - 3s 102ms/step - loss: 5.2214 - mae: 1.6784 - val_loss: 5.5529 - val_mae: 1.7154
    Epoch 49/1000
    26/26 [==============================] - 3s 99ms/step - loss: 4.9379 - mae: 1.6392 - val_loss: 4.5321 - val_mae: 1.5689
    Epoch 50/1000
    26/26 [==============================] - 3s 100ms/step - loss: 5.3071 - mae: 1.6870 - val_loss: 5.1403 - val_mae: 1.6691
    Epoch 51/1000
    26/26 [==============================] - 3s 100ms/step - loss: 4.7460 - mae: 1.5985 - val_loss: 4.7076 - val_mae: 1.6088
    Epoch 52/1000
    26/26 [==============================] - 3s 101ms/step - loss: 4.3934 - mae: 1.5609 - val_loss: 4.3986 - val_mae: 1.5668
    Epoch 53/1000
    26/26 [==============================] - 3s 100ms/step - loss: 4.5698 - mae: 1.5973 - val_loss: 4.9158 - val_mae: 1.6187
    Epoch 54/1000
    26/26 [==============================] - 4s 102ms/step - loss: 4.6844 - mae: 1.6056 - val_loss: 4.5686 - val_mae: 1.5759
    Epoch 55/1000
    26/26 [==============================] - 3s 99ms/step - loss: 3.8894 - mae: 1.4856 - val_loss: 3.5969 - val_mae: 1.4492
    Epoch 56/1000
    26/26 [==============================] - 4s 104ms/step - loss: 4.0116 - mae: 1.4846 - val_loss: 4.6730 - val_mae: 1.5948
    Epoch 57/1000
    26/26 [==============================] - 4s 102ms/step - loss: 3.3933 - mae: 1.3912 - val_loss: 3.8705 - val_mae: 1.4859
    Epoch 58/1000
    26/26 [==============================] - 4s 104ms/step - loss: 3.5336 - mae: 1.4390 - val_loss: 3.7981 - val_mae: 1.4721
    Epoch 59/1000
    26/26 [==============================] - 3s 102ms/step - loss: 3.8131 - mae: 1.4814 - val_loss: 4.3450 - val_mae: 1.5560
    Epoch 60/1000
    26/26 [==============================] - 3s 100ms/step - loss: 2.9246 - mae: 1.3489 - val_loss: 4.1717 - val_mae: 1.5251
    Epoch 61/1000
    26/26 [==============================] - 4s 103ms/step - loss: 4.1061 - mae: 1.5225 - val_loss: 3.1565 - val_mae: 1.3823
    Epoch 62/1000
    26/26 [==============================] - 4s 105ms/step - loss: 3.9634 - mae: 1.4972 - val_loss: 3.8334 - val_mae: 1.4621
    Epoch 63/1000
    26/26 [==============================] - 3s 103ms/step - loss: 3.9076 - mae: 1.4784 - val_loss: 3.0957 - val_mae: 1.3849
    Epoch 64/1000
    26/26 [==============================] - 4s 106ms/step - loss: 2.9699 - mae: 1.3548 - val_loss: 2.3125 - val_mae: 1.2589
    Epoch 65/1000
    26/26 [==============================] - 3s 102ms/step - loss: 2.9961 - mae: 1.3383 - val_loss: 3.2733 - val_mae: 1.3799
    Epoch 66/1000
    26/26 [==============================] - 3s 101ms/step - loss: 2.7627 - mae: 1.2775 - val_loss: 2.9063 - val_mae: 1.3323
    Epoch 67/1000
    26/26 [==============================] - 3s 99ms/step - loss: 2.6513 - mae: 1.3207 - val_loss: 3.0464 - val_mae: 1.3699
    Epoch 68/1000
    26/26 [==============================] - 3s 100ms/step - loss: 3.3908 - mae: 1.4262 - val_loss: 2.6225 - val_mae: 1.2900
    Epoch 69/1000
    26/26 [==============================] - 3s 100ms/step - loss: 2.8709 - mae: 1.3378 - val_loss: 3.0987 - val_mae: 1.3786
    Epoch 70/1000
    26/26 [==============================] - 5s 147ms/step - loss: 2.0885 - mae: 1.2032 - val_loss: 2.7070 - val_mae: 1.3003
    Epoch 71/1000
    26/26 [==============================] - 4s 106ms/step - loss: 2.4402 - mae: 1.2730 - val_loss: 2.5223 - val_mae: 1.3007
    Epoch 72/1000
    26/26 [==============================] - 4s 102ms/step - loss: 2.4578 - mae: 1.2750 - val_loss: 2.7908 - val_mae: 1.3421
    Epoch 73/1000
    26/26 [==============================] - 3s 100ms/step - loss: 2.3705 - mae: 1.2666 - val_loss: 2.2546 - val_mae: 1.2284
    Epoch 74/1000
    26/26 [==============================] - 3s 99ms/step - loss: 2.5738 - mae: 1.2772 - val_loss: 2.5808 - val_mae: 1.2756
    Epoch 75/1000
    26/26 [==============================] - 4s 98ms/step - loss: 1.5251 - mae: 1.0865 - val_loss: 2.1148 - val_mae: 1.2053
    Epoch 76/1000
    26/26 [==============================] - 5s 156ms/step - loss: 2.2507 - mae: 1.2458 - val_loss: 2.7488 - val_mae: 1.3157
    Epoch 77/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.9511 - mae: 1.1358 - val_loss: 2.4230 - val_mae: 1.2842
    Epoch 78/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.8122 - mae: 1.1574 - val_loss: 1.8152 - val_mae: 1.1543
    Epoch 79/1000
    26/26 [==============================] - 3s 98ms/step - loss: 2.3801 - mae: 1.2327 - val_loss: 2.2829 - val_mae: 1.2468
    Epoch 80/1000
    26/26 [==============================] - 3s 103ms/step - loss: 1.6741 - mae: 1.1111 - val_loss: 1.8695 - val_mae: 1.1298
    Epoch 81/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.7122 - mae: 1.1045 - val_loss: 1.6359 - val_mae: 1.0870
    Epoch 82/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.6575 - mae: 1.0942 - val_loss: 1.8365 - val_mae: 1.1287
    Epoch 83/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.5425 - mae: 1.0781 - val_loss: 2.1055 - val_mae: 1.2077
    Epoch 84/1000
    26/26 [==============================] - 4s 104ms/step - loss: 2.0043 - mae: 1.1799 - val_loss: 1.9051 - val_mae: 1.1432
    Epoch 85/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.9846 - mae: 1.1993 - val_loss: 1.8261 - val_mae: 1.1577
    Epoch 86/1000
    26/26 [==============================] - 3s 98ms/step - loss: 1.8164 - mae: 1.1366 - val_loss: 2.2132 - val_mae: 1.2013
    Epoch 87/1000
    26/26 [==============================] - 3s 102ms/step - loss: 2.0471 - mae: 1.2070 - val_loss: 1.8779 - val_mae: 1.1340
    Epoch 88/1000
    26/26 [==============================] - 5s 145ms/step - loss: 1.6653 - mae: 1.0993 - val_loss: 1.6727 - val_mae: 1.0699
    Epoch 89/1000
    26/26 [==============================] - 3s 98ms/step - loss: 1.5254 - mae: 1.0642 - val_loss: 1.6075 - val_mae: 1.0761
    Epoch 90/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.5458 - mae: 1.0556 - val_loss: 1.7021 - val_mae: 1.0996
    Epoch 91/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.5332 - mae: 1.0775 - val_loss: 1.6809 - val_mae: 1.0993
    Epoch 92/1000
    26/26 [==============================] - 3s 96ms/step - loss: 1.3696 - mae: 1.0373 - val_loss: 1.9049 - val_mae: 1.1465
    Epoch 93/1000
    26/26 [==============================] - 3s 99ms/step - loss: 1.5511 - mae: 1.0809 - val_loss: 1.6915 - val_mae: 1.0949
    Epoch 94/1000
    26/26 [==============================] - 3s 99ms/step - loss: 1.5702 - mae: 1.0820 - val_loss: 1.3457 - val_mae: 1.0082
    Epoch 95/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.6627 - mae: 1.0677 - val_loss: 1.5390 - val_mae: 1.0392
    Epoch 96/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.4203 - mae: 1.0215 - val_loss: 1.1813 - val_mae: 0.9187
    Epoch 97/1000
    26/26 [==============================] - 4s 102ms/step - loss: 1.5895 - mae: 1.1097 - val_loss: 1.4840 - val_mae: 1.0840
    Epoch 98/1000
    26/26 [==============================] - 4s 100ms/step - loss: 1.2028 - mae: 0.9303 - val_loss: 1.4680 - val_mae: 1.0600
    Epoch 99/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.6729 - mae: 1.0838 - val_loss: 1.3096 - val_mae: 0.9764
    Epoch 100/1000
    26/26 [==============================] - 3s 98ms/step - loss: 1.4740 - mae: 1.0896 - val_loss: 1.5543 - val_mae: 1.0646
    Epoch 101/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.4587 - mae: 1.0295 - val_loss: 1.4164 - val_mae: 1.0019
    Epoch 102/1000
    26/26 [==============================] - 3s 99ms/step - loss: 1.3079 - mae: 1.0105 - val_loss: 1.2458 - val_mae: 0.9455
    Epoch 103/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.3777 - mae: 1.0083 - val_loss: 1.5113 - val_mae: 1.0563
    Epoch 104/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.0955 - mae: 0.9339 - val_loss: 1.4283 - val_mae: 1.0268
    Epoch 105/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.3298 - mae: 0.9993 - val_loss: 1.4625 - val_mae: 1.0583
    Epoch 106/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.3613 - mae: 1.0091 - val_loss: 1.4732 - val_mae: 0.9978
    Epoch 107/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.0766 - mae: 0.9016 - val_loss: 1.2852 - val_mae: 1.0163
    Epoch 108/1000
    26/26 [==============================] - 3s 102ms/step - loss: 1.3633 - mae: 0.9949 - val_loss: 1.3046 - val_mae: 0.9769
    Epoch 109/1000
    26/26 [==============================] - 4s 103ms/step - loss: 1.5773 - mae: 1.1051 - val_loss: 1.3364 - val_mae: 0.9885
    Epoch 110/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.4091 - mae: 1.0299 - val_loss: 1.2530 - val_mae: 0.9649
    Epoch 111/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.3859 - mae: 1.0192 - val_loss: 1.3821 - val_mae: 1.0336
    Epoch 112/1000
    26/26 [==============================] - 3s 103ms/step - loss: 1.2842 - mae: 0.9924 - val_loss: 1.2568 - val_mae: 1.0237
    Epoch 113/1000
    26/26 [==============================] - 4s 116ms/step - loss: 1.2141 - mae: 0.9589 - val_loss: 1.2108 - val_mae: 0.9667
    Epoch 114/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.2344 - mae: 0.9976 - val_loss: 1.1030 - val_mae: 0.9134
    Epoch 115/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.0917 - mae: 0.9394 - val_loss: 1.1891 - val_mae: 0.9517
    Epoch 116/1000
    26/26 [==============================] - 4s 106ms/step - loss: 1.1721 - mae: 0.9529 - val_loss: 1.3297 - val_mae: 0.9982
    Epoch 117/1000
    26/26 [==============================] - 3s 102ms/step - loss: 1.0808 - mae: 0.9030 - val_loss: 1.2875 - val_mae: 0.9667
    Epoch 118/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.2022 - mae: 0.9501 - val_loss: 1.2721 - val_mae: 1.0008
    Epoch 119/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.3529 - mae: 1.0447 - val_loss: 1.3058 - val_mae: 1.0219
    Epoch 120/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.2396 - mae: 0.9678 - val_loss: 1.1364 - val_mae: 0.9823
    Epoch 121/1000
    26/26 [==============================] - 4s 108ms/step - loss: 1.1858 - mae: 0.9606 - val_loss: 1.2915 - val_mae: 0.9898
    Epoch 122/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.0805 - mae: 0.9213 - val_loss: 1.1015 - val_mae: 0.9079
    Epoch 123/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.1932 - mae: 0.9726 - val_loss: 1.3780 - val_mae: 1.0272
    Epoch 124/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.1724 - mae: 0.9549 - val_loss: 1.0894 - val_mae: 0.9011
    Epoch 125/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.0796 - mae: 0.9295 - val_loss: 1.0884 - val_mae: 0.9398
    Epoch 126/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.1688 - mae: 0.9401 - val_loss: 1.0169 - val_mae: 0.9215
    Epoch 127/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.1884 - mae: 0.9498 - val_loss: 1.1345 - val_mae: 0.9297
    Epoch 128/1000
    26/26 [==============================] - 4s 111ms/step - loss: 1.1361 - mae: 0.9408 - val_loss: 1.2137 - val_mae: 0.9826
    Epoch 129/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9292 - mae: 0.8726 - val_loss: 1.0493 - val_mae: 0.9316
    Epoch 130/1000
    26/26 [==============================] - 4s 118ms/step - loss: 1.2555 - mae: 0.9938 - val_loss: 1.0746 - val_mae: 0.9422
    Epoch 131/1000
    26/26 [==============================] - 4s 101ms/step - loss: 1.1099 - mae: 0.9481 - val_loss: 1.1314 - val_mae: 0.9300
    Epoch 132/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.9791 - mae: 0.9051 - val_loss: 1.0987 - val_mae: 0.9309
    Epoch 133/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.1560 - mae: 0.9473 - val_loss: 1.0206 - val_mae: 0.8681
    Epoch 134/1000
    26/26 [==============================] - 5s 147ms/step - loss: 1.0663 - mae: 0.8963 - val_loss: 0.9885 - val_mae: 0.9059
    Epoch 135/1000
    26/26 [==============================] - 3s 102ms/step - loss: 1.0668 - mae: 0.9286 - val_loss: 1.2037 - val_mae: 0.9552
    Epoch 136/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.9787 - mae: 0.8608 - val_loss: 1.3810 - val_mae: 1.0388
    Epoch 137/1000
    26/26 [==============================] - 4s 108ms/step - loss: 1.1553 - mae: 0.9401 - val_loss: 1.3401 - val_mae: 1.0131
    Epoch 138/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.0838 - mae: 0.9221 - val_loss: 1.0604 - val_mae: 0.9265
    Epoch 139/1000
    26/26 [==============================] - 4s 111ms/step - loss: 1.0084 - mae: 0.8738 - val_loss: 1.0682 - val_mae: 0.9409
    Epoch 140/1000
    26/26 [==============================] - 4s 106ms/step - loss: 1.0624 - mae: 0.9204 - val_loss: 0.9643 - val_mae: 0.8966
    Epoch 141/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.9423 - mae: 0.8671 - val_loss: 0.9001 - val_mae: 0.8526
    Epoch 142/1000
    26/26 [==============================] - 4s 106ms/step - loss: 1.1404 - mae: 0.9285 - val_loss: 0.9898 - val_mae: 0.8840
    Epoch 143/1000
    26/26 [==============================] - 4s 108ms/step - loss: 1.0429 - mae: 0.9228 - val_loss: 0.9011 - val_mae: 0.8608
    Epoch 144/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9365 - mae: 0.8457 - val_loss: 0.7717 - val_mae: 0.7939
    Epoch 145/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.9688 - mae: 0.8886 - val_loss: 1.2556 - val_mae: 0.9607
    Epoch 146/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.9716 - mae: 0.8720 - val_loss: 1.2635 - val_mae: 0.9823
    Epoch 147/1000
    26/26 [==============================] - 4s 102ms/step - loss: 1.0231 - mae: 0.8938 - val_loss: 0.9672 - val_mae: 0.8855
    Epoch 148/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.9843 - mae: 0.8714 - val_loss: 0.9818 - val_mae: 0.8728
    Epoch 149/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.9959 - mae: 0.9013 - val_loss: 0.9500 - val_mae: 0.8877
    Epoch 150/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8361 - mae: 0.8142 - val_loss: 0.8844 - val_mae: 0.8242
    Epoch 151/1000
    26/26 [==============================] - 4s 111ms/step - loss: 0.8302 - mae: 0.8286 - val_loss: 1.2850 - val_mae: 0.9861
    Epoch 152/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.8942 - mae: 0.8359 - val_loss: 0.9794 - val_mae: 0.8528
    Epoch 153/1000
    26/26 [==============================] - 4s 108ms/step - loss: 1.0656 - mae: 0.9224 - val_loss: 1.0046 - val_mae: 0.8768
    Epoch 154/1000
    26/26 [==============================] - 4s 102ms/step - loss: 1.0584 - mae: 0.9182 - val_loss: 1.0587 - val_mae: 0.8828
    Epoch 155/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.9550 - mae: 0.8737 - val_loss: 0.9402 - val_mae: 0.8446
    Epoch 156/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.9819 - mae: 0.9031 - val_loss: 1.0435 - val_mae: 0.9040
    Epoch 157/1000
    26/26 [==============================] - 4s 101ms/step - loss: 1.1395 - mae: 0.9200 - val_loss: 0.9747 - val_mae: 0.8492
    Epoch 158/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.8696 - mae: 0.8185 - val_loss: 0.8594 - val_mae: 0.8107
    Epoch 159/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.1120 - mae: 0.9434 - val_loss: 0.9638 - val_mae: 0.8494
    Epoch 160/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.8837 - mae: 0.8341 - val_loss: 0.8794 - val_mae: 0.8024
    Epoch 161/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9443 - mae: 0.8369 - val_loss: 0.8467 - val_mae: 0.8454
    Epoch 162/1000
    26/26 [==============================] - 3s 100ms/step - loss: 1.0160 - mae: 0.8740 - val_loss: 1.0414 - val_mae: 0.8691
    Epoch 163/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9462 - mae: 0.8525 - val_loss: 0.7490 - val_mae: 0.7874
    Epoch 164/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8862 - mae: 0.8351 - val_loss: 0.9000 - val_mae: 0.8162
    Epoch 165/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8592 - mae: 0.8225 - val_loss: 0.9759 - val_mae: 0.8511
    Epoch 166/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.8272 - mae: 0.8186 - val_loss: 0.8518 - val_mae: 0.8014
    Epoch 167/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.9507 - mae: 0.8548 - val_loss: 0.8738 - val_mae: 0.8015
    Epoch 168/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.9259 - mae: 0.8552 - val_loss: 0.9831 - val_mae: 0.8974
    Epoch 169/1000
    26/26 [==============================] - 5s 159ms/step - loss: 0.9390 - mae: 0.8545 - val_loss: 1.1086 - val_mae: 0.8901
    Epoch 170/1000
    26/26 [==============================] - 4s 109ms/step - loss: 0.8350 - mae: 0.8096 - val_loss: 0.8936 - val_mae: 0.8700
    Epoch 171/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.9508 - mae: 0.8408 - val_loss: 0.7729 - val_mae: 0.7618
    Epoch 172/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.0210 - mae: 0.8662 - val_loss: 0.8974 - val_mae: 0.8338
    Epoch 173/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7876 - mae: 0.7838 - val_loss: 0.8461 - val_mae: 0.8209
    Epoch 174/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9172 - mae: 0.8027 - val_loss: 0.9855 - val_mae: 0.8356
    Epoch 175/1000
    26/26 [==============================] - 4s 113ms/step - loss: 1.0115 - mae: 0.8508 - val_loss: 0.8127 - val_mae: 0.7975
    Epoch 176/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7522 - mae: 0.7514 - val_loss: 0.8344 - val_mae: 0.8083
    Epoch 177/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.9069 - mae: 0.8433 - val_loss: 0.9079 - val_mae: 0.8363
    Epoch 178/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8291 - mae: 0.8212 - val_loss: 1.0852 - val_mae: 0.8897
    Epoch 179/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.0530 - mae: 0.8772 - val_loss: 0.8275 - val_mae: 0.7956
    Epoch 180/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8743 - mae: 0.8016 - val_loss: 0.7813 - val_mae: 0.7955
    Epoch 181/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.9142 - mae: 0.8306 - val_loss: 0.7842 - val_mae: 0.7909
    Epoch 182/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8107 - mae: 0.7724 - val_loss: 0.8420 - val_mae: 0.8069
    Epoch 183/1000
    26/26 [==============================] - 4s 103ms/step - loss: 1.1211 - mae: 0.8450 - val_loss: 0.8853 - val_mae: 0.8283
    Epoch 184/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.9747 - mae: 0.8282 - val_loss: 0.9752 - val_mae: 0.8454
    Epoch 185/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.9617 - mae: 0.8698 - val_loss: 0.9485 - val_mae: 0.8466
    Epoch 186/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.9480 - mae: 0.8533 - val_loss: 0.9808 - val_mae: 0.8659
    Epoch 187/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8841 - mae: 0.8645 - val_loss: 0.8362 - val_mae: 0.7998
    Epoch 188/1000
    26/26 [==============================] - 4s 103ms/step - loss: 1.0978 - mae: 0.9123 - val_loss: 0.8679 - val_mae: 0.8529
    Epoch 189/1000
    26/26 [==============================] - 4s 101ms/step - loss: 1.0043 - mae: 0.8630 - val_loss: 0.8085 - val_mae: 0.8054
    Epoch 190/1000
    26/26 [==============================] - 3s 99ms/step - loss: 0.9842 - mae: 0.8137 - val_loss: 0.7449 - val_mae: 0.7776
    Epoch 191/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8878 - mae: 0.8198 - val_loss: 0.7336 - val_mae: 0.7448
    Epoch 192/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7279 - mae: 0.7552 - val_loss: 0.6967 - val_mae: 0.7252
    Epoch 193/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9306 - mae: 0.8104 - val_loss: 0.7775 - val_mae: 0.7820
    Epoch 194/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7844 - mae: 0.7796 - val_loss: 0.7734 - val_mae: 0.7778
    Epoch 195/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.9660 - mae: 0.8534 - val_loss: 0.9361 - val_mae: 0.8180
    Epoch 196/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.7644 - mae: 0.7551 - val_loss: 0.7086 - val_mae: 0.7179
    Epoch 197/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.8012 - mae: 0.7522 - val_loss: 0.7183 - val_mae: 0.6986
    Epoch 198/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.7861 - mae: 0.7775 - val_loss: 1.0147 - val_mae: 0.8342
    Epoch 199/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7143 - mae: 0.7182 - val_loss: 0.7002 - val_mae: 0.7222
    Epoch 200/1000
    26/26 [==============================] - 5s 166ms/step - loss: 0.8373 - mae: 0.7946 - val_loss: 0.9721 - val_mae: 0.8466
    Epoch 201/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6472 - mae: 0.7102 - val_loss: 0.8058 - val_mae: 0.7598
    Epoch 202/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8789 - mae: 0.8252 - val_loss: 0.8992 - val_mae: 0.8434
    Epoch 203/1000
    26/26 [==============================] - 3s 99ms/step - loss: 0.8323 - mae: 0.7843 - val_loss: 0.7431 - val_mae: 0.7384
    Epoch 204/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8450 - mae: 0.8201 - val_loss: 0.9089 - val_mae: 0.8009
    Epoch 205/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.8713 - mae: 0.7722 - val_loss: 0.8600 - val_mae: 0.7662
    Epoch 206/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6772 - mae: 0.7091 - val_loss: 0.7684 - val_mae: 0.7367
    Epoch 207/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6166 - mae: 0.7275 - val_loss: 0.7150 - val_mae: 0.7338
    Epoch 208/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.8118 - mae: 0.7642 - val_loss: 0.9648 - val_mae: 0.8070
    Epoch 209/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.8415 - mae: 0.7750 - val_loss: 0.6614 - val_mae: 0.7049
    Epoch 210/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8613 - mae: 0.7669 - val_loss: 0.7489 - val_mae: 0.7435
    Epoch 211/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8114 - mae: 0.7972 - val_loss: 0.7848 - val_mae: 0.7585
    Epoch 212/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9637 - mae: 0.8115 - val_loss: 0.8092 - val_mae: 0.7370
    Epoch 213/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8124 - mae: 0.7523 - val_loss: 0.9259 - val_mae: 0.7944
    Epoch 214/1000
    26/26 [==============================] - 4s 109ms/step - loss: 1.0786 - mae: 0.9080 - val_loss: 0.7703 - val_mae: 0.7142
    Epoch 215/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.6478 - mae: 0.6963 - val_loss: 0.8811 - val_mae: 0.7890
    Epoch 216/1000
    26/26 [==============================] - 3s 99ms/step - loss: 0.8480 - mae: 0.7953 - val_loss: 0.8759 - val_mae: 0.7838
    Epoch 217/1000
    26/26 [==============================] - 4s 119ms/step - loss: 0.8216 - mae: 0.7167 - val_loss: 0.8530 - val_mae: 0.7789
    Epoch 218/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8346 - mae: 0.7291 - val_loss: 0.8767 - val_mae: 0.7799
    Epoch 219/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.9231 - mae: 0.7815 - val_loss: 0.8703 - val_mae: 0.7752
    Epoch 220/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.9220 - mae: 0.7793 - val_loss: 0.6198 - val_mae: 0.6621
    Epoch 221/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6768 - mae: 0.7300 - val_loss: 1.0934 - val_mae: 0.8673
    Epoch 222/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8326 - mae: 0.7723 - val_loss: 0.7797 - val_mae: 0.7492
    Epoch 223/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.9308 - mae: 0.7991 - val_loss: 0.8257 - val_mae: 0.7462
    Epoch 224/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5978 - mae: 0.6799 - val_loss: 0.5753 - val_mae: 0.6679
    Epoch 225/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6387 - mae: 0.6841 - val_loss: 0.8694 - val_mae: 0.7745
    Epoch 226/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7409 - mae: 0.7326 - val_loss: 0.9695 - val_mae: 0.8112
    Epoch 227/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8343 - mae: 0.7648 - val_loss: 0.6623 - val_mae: 0.6932
    Epoch 228/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7783 - mae: 0.7444 - val_loss: 0.8713 - val_mae: 0.7403
    Epoch 229/1000
    26/26 [==============================] - 4s 125ms/step - loss: 1.0491 - mae: 0.8737 - val_loss: 0.9076 - val_mae: 0.7992
    Epoch 230/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8446 - mae: 0.7791 - val_loss: 0.6367 - val_mae: 0.6241
    Epoch 231/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.7897 - mae: 0.6982 - val_loss: 0.7588 - val_mae: 0.7673
    Epoch 232/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5490 - mae: 0.6227 - val_loss: 0.8591 - val_mae: 0.7230
    Epoch 233/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7416 - mae: 0.7441 - val_loss: 0.9549 - val_mae: 0.8049
    Epoch 234/1000
    26/26 [==============================] - 5s 147ms/step - loss: 0.8265 - mae: 0.7614 - val_loss: 0.9046 - val_mae: 0.7783
    Epoch 235/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.7567 - mae: 0.7103 - val_loss: 1.0954 - val_mae: 0.8579
    Epoch 236/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.8728 - mae: 0.7659 - val_loss: 0.6768 - val_mae: 0.7125
    Epoch 237/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.7572 - mae: 0.7342 - val_loss: 0.8110 - val_mae: 0.7593
    Epoch 238/1000
    26/26 [==============================] - 5s 145ms/step - loss: 0.9990 - mae: 0.8210 - val_loss: 1.0940 - val_mae: 0.7945
    Epoch 239/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8854 - mae: 0.7642 - val_loss: 0.9139 - val_mae: 0.7823
    Epoch 240/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7599 - mae: 0.6995 - val_loss: 0.7363 - val_mae: 0.6782
    Epoch 241/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.6470 - mae: 0.6874 - val_loss: 0.6732 - val_mae: 0.6718
    Epoch 242/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.7953 - mae: 0.7286 - val_loss: 0.7919 - val_mae: 0.7372
    Epoch 243/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7243 - mae: 0.6552 - val_loss: 0.7481 - val_mae: 0.6986
    Epoch 244/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8901 - mae: 0.7866 - val_loss: 0.7789 - val_mae: 0.7334
    Epoch 245/1000
    26/26 [==============================] - 5s 154ms/step - loss: 0.8195 - mae: 0.7565 - val_loss: 0.9467 - val_mae: 0.8445
    Epoch 246/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.5206 - mae: 0.6384 - val_loss: 0.9272 - val_mae: 0.7827
    Epoch 247/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8027 - mae: 0.7687 - val_loss: 0.6419 - val_mae: 0.6791
    Epoch 248/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8782 - mae: 0.7138 - val_loss: 0.6095 - val_mae: 0.6416
    Epoch 249/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7965 - mae: 0.7408 - val_loss: 0.7752 - val_mae: 0.7266
    Epoch 250/1000
    26/26 [==============================] - 4s 109ms/step - loss: 0.8660 - mae: 0.7769 - val_loss: 0.9333 - val_mae: 0.7536
    Epoch 251/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7023 - mae: 0.6836 - val_loss: 0.8160 - val_mae: 0.7384
    Epoch 252/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.4759 - mae: 0.5822 - val_loss: 0.9675 - val_mae: 0.8013
    Epoch 253/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.7377 - mae: 0.7108 - val_loss: 0.6768 - val_mae: 0.6817
    Epoch 254/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7612 - mae: 0.7047 - val_loss: 0.6643 - val_mae: 0.7044
    Epoch 255/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9772 - mae: 0.7758 - val_loss: 0.6327 - val_mae: 0.6794
    Epoch 256/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8180 - mae: 0.7242 - val_loss: 0.7640 - val_mae: 0.7484
    Epoch 257/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.7480 - mae: 0.6843 - val_loss: 0.7654 - val_mae: 0.7072
    Epoch 258/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7467 - mae: 0.6994 - val_loss: 0.9641 - val_mae: 0.7874
    Epoch 259/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.9793 - mae: 0.7838 - val_loss: 0.6445 - val_mae: 0.6293
    Epoch 260/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8515 - mae: 0.7325 - val_loss: 0.8799 - val_mae: 0.7386
    Epoch 261/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.5518 - mae: 0.6228 - val_loss: 0.7582 - val_mae: 0.6961
    Epoch 262/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7896 - mae: 0.7049 - val_loss: 0.6246 - val_mae: 0.6793
    Epoch 263/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.5761 - mae: 0.6210 - val_loss: 0.8522 - val_mae: 0.7427
    Epoch 264/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6671 - mae: 0.6891 - val_loss: 0.4139 - val_mae: 0.5204
    Epoch 265/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.6238 - mae: 0.6736 - val_loss: 0.9946 - val_mae: 0.7668
    Epoch 266/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.9304 - mae: 0.8060 - val_loss: 0.6707 - val_mae: 0.6920
    Epoch 267/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.7997 - mae: 0.7235 - val_loss: 0.9114 - val_mae: 0.7469
    Epoch 268/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6571 - mae: 0.6817 - val_loss: 0.8198 - val_mae: 0.6921
    Epoch 269/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8906 - mae: 0.7664 - val_loss: 0.8252 - val_mae: 0.7712
    Epoch 270/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.8196 - mae: 0.7458 - val_loss: 0.9367 - val_mae: 0.7779
    Epoch 271/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5796 - mae: 0.6488 - val_loss: 0.9061 - val_mae: 0.7856
    Epoch 272/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.7339 - mae: 0.6931 - val_loss: 0.7426 - val_mae: 0.7387
    Epoch 273/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6082 - mae: 0.6542 - val_loss: 0.5942 - val_mae: 0.5707
    Epoch 274/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6937 - mae: 0.7014 - val_loss: 0.5631 - val_mae: 0.6299
    Epoch 275/1000
    26/26 [==============================] - 3s 99ms/step - loss: 1.0307 - mae: 0.7687 - val_loss: 0.6675 - val_mae: 0.6531
    Epoch 276/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.5676 - mae: 0.6103 - val_loss: 1.0203 - val_mae: 0.8433
    Epoch 277/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.9598 - mae: 0.7869 - val_loss: 0.7725 - val_mae: 0.6955
    Epoch 278/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.7667 - mae: 0.6808 - val_loss: 0.8876 - val_mae: 0.7544
    Epoch 279/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.7689 - mae: 0.6983 - val_loss: 0.8884 - val_mae: 0.7495
    Epoch 280/1000
    26/26 [==============================] - 4s 109ms/step - loss: 0.5432 - mae: 0.5795 - val_loss: 0.5071 - val_mae: 0.5561
    Epoch 281/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6682 - mae: 0.5710 - val_loss: 0.5219 - val_mae: 0.5724
    Epoch 282/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7011 - mae: 0.6621 - val_loss: 0.6254 - val_mae: 0.6517
    Epoch 283/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6470 - mae: 0.6637 - val_loss: 1.1449 - val_mae: 0.8248
    Epoch 284/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7922 - mae: 0.7368 - val_loss: 0.8687 - val_mae: 0.7637
    Epoch 285/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5098 - mae: 0.5805 - val_loss: 0.6379 - val_mae: 0.6117
    Epoch 286/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6135 - mae: 0.6305 - val_loss: 0.7452 - val_mae: 0.7272
    Epoch 287/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7833 - mae: 0.7638 - val_loss: 0.9103 - val_mae: 0.7951
    Epoch 288/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.7627 - mae: 0.6958 - val_loss: 0.9624 - val_mae: 0.7697
    Epoch 289/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7501 - mae: 0.7178 - val_loss: 0.8779 - val_mae: 0.7363
    Epoch 290/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.4513 - mae: 0.5574 - val_loss: 0.6241 - val_mae: 0.6405
    Epoch 291/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9511 - mae: 0.7853 - val_loss: 0.5994 - val_mae: 0.6209
    Epoch 292/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7429 - mae: 0.6944 - val_loss: 0.7100 - val_mae: 0.6508
    Epoch 293/1000
    26/26 [==============================] - 5s 150ms/step - loss: 0.7314 - mae: 0.6450 - val_loss: 0.7445 - val_mae: 0.6889
    Epoch 294/1000
    26/26 [==============================] - 3s 104ms/step - loss: 0.8972 - mae: 0.7325 - val_loss: 0.6242 - val_mae: 0.5988
    Epoch 295/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.7324 - mae: 0.6977 - val_loss: 0.5468 - val_mae: 0.6097
    Epoch 296/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5768 - mae: 0.6223 - val_loss: 0.5266 - val_mae: 0.5764
    Epoch 297/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6005 - mae: 0.6266 - val_loss: 0.9035 - val_mae: 0.7362
    Epoch 298/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7153 - mae: 0.6831 - val_loss: 0.6898 - val_mae: 0.6849
    Epoch 299/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8117 - mae: 0.7330 - val_loss: 0.5048 - val_mae: 0.5625
    Epoch 300/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.6471 - mae: 0.6353 - val_loss: 0.7085 - val_mae: 0.6524
    Epoch 301/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6399 - mae: 0.6156 - val_loss: 0.6378 - val_mae: 0.6342
    Epoch 302/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.7155 - mae: 0.6810 - val_loss: 0.9265 - val_mae: 0.7468
    Epoch 303/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6737 - mae: 0.6203 - val_loss: 0.7071 - val_mae: 0.6772
    Epoch 304/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.8302 - mae: 0.7512 - val_loss: 0.6559 - val_mae: 0.6343
    Epoch 305/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.5966 - mae: 0.6201 - val_loss: 0.6127 - val_mae: 0.6294
    Epoch 306/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7082 - mae: 0.6932 - val_loss: 0.5874 - val_mae: 0.6288
    Epoch 307/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6449 - mae: 0.6538 - val_loss: 0.8817 - val_mae: 0.7177
    Epoch 308/1000
    26/26 [==============================] - 4s 101ms/step - loss: 1.0065 - mae: 0.7680 - val_loss: 1.1076 - val_mae: 0.8429
    Epoch 309/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6259 - mae: 0.6351 - val_loss: 0.6228 - val_mae: 0.6237
    Epoch 310/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8348 - mae: 0.6811 - val_loss: 0.6496 - val_mae: 0.6439
    Epoch 311/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5749 - mae: 0.6158 - val_loss: 0.6725 - val_mae: 0.6256
    Epoch 312/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5960 - mae: 0.6021 - val_loss: 0.5175 - val_mae: 0.5673
    Epoch 313/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.5639 - mae: 0.5938 - val_loss: 0.7100 - val_mae: 0.6737
    Epoch 314/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9978 - mae: 0.8243 - val_loss: 0.6828 - val_mae: 0.6906
    Epoch 315/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6089 - mae: 0.6065 - val_loss: 0.8752 - val_mae: 0.7098
    Epoch 316/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.9096 - mae: 0.7249 - val_loss: 0.6667 - val_mae: 0.6631
    Epoch 317/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6011 - mae: 0.6522 - val_loss: 1.0871 - val_mae: 0.8395
    Epoch 318/1000
    26/26 [==============================] - 4s 111ms/step - loss: 0.4803 - mae: 0.5653 - val_loss: 0.9182 - val_mae: 0.7219
    Epoch 319/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.6170 - mae: 0.6167 - val_loss: 0.6662 - val_mae: 0.6139
    Epoch 320/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8042 - mae: 0.6984 - val_loss: 0.8525 - val_mae: 0.7221
    Epoch 321/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6980 - mae: 0.6656 - val_loss: 0.9041 - val_mae: 0.7639
    Epoch 322/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.6886 - mae: 0.6494 - val_loss: 1.0284 - val_mae: 0.7748
    Epoch 323/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8924 - mae: 0.7430 - val_loss: 0.6859 - val_mae: 0.6621
    Epoch 324/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.7763 - mae: 0.6707 - val_loss: 0.9819 - val_mae: 0.7650
    Epoch 325/1000
    26/26 [==============================] - 5s 145ms/step - loss: 0.9300 - mae: 0.7096 - val_loss: 0.5427 - val_mae: 0.6105
    Epoch 326/1000
    26/26 [==============================] - 4s 111ms/step - loss: 0.4839 - mae: 0.5463 - val_loss: 0.8838 - val_mae: 0.7156
    Epoch 327/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8286 - mae: 0.6908 - val_loss: 0.7468 - val_mae: 0.7102
    Epoch 328/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.5839 - mae: 0.6030 - val_loss: 0.7501 - val_mae: 0.6339
    Epoch 329/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.7283 - mae: 0.6256 - val_loss: 0.5749 - val_mae: 0.6008
    Epoch 330/1000
    26/26 [==============================] - 3s 96ms/step - loss: 0.6687 - mae: 0.6351 - val_loss: 0.8629 - val_mae: 0.7220
    Epoch 331/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8263 - mae: 0.7242 - val_loss: 0.8394 - val_mae: 0.7047
    Epoch 332/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.8043 - mae: 0.7343 - val_loss: 0.7713 - val_mae: 0.6995
    Epoch 333/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7953 - mae: 0.6723 - val_loss: 0.7961 - val_mae: 0.6734
    Epoch 334/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.8271 - mae: 0.7401 - val_loss: 0.8135 - val_mae: 0.6612
    Epoch 335/1000
    26/26 [==============================] - 3s 104ms/step - loss: 0.7375 - mae: 0.6877 - val_loss: 0.7169 - val_mae: 0.6587
    Epoch 336/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5552 - mae: 0.5902 - val_loss: 0.7457 - val_mae: 0.6443
    Epoch 337/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5281 - mae: 0.5709 - val_loss: 0.6073 - val_mae: 0.6053
    Epoch 338/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5923 - mae: 0.6204 - val_loss: 0.4056 - val_mae: 0.5028
    Epoch 339/1000
    26/26 [==============================] - 4s 129ms/step - loss: 0.8295 - mae: 0.7329 - val_loss: 0.5252 - val_mae: 0.5560
    Epoch 340/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7657 - mae: 0.6711 - val_loss: 0.6459 - val_mae: 0.6297
    Epoch 341/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6116 - mae: 0.6160 - val_loss: 0.7915 - val_mae: 0.6842
    Epoch 342/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8154 - mae: 0.6474 - val_loss: 0.6138 - val_mae: 0.6172
    Epoch 343/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7940 - mae: 0.7087 - val_loss: 0.8658 - val_mae: 0.7147
    Epoch 344/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7016 - mae: 0.6983 - val_loss: 0.8759 - val_mae: 0.7232
    Epoch 345/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6453 - mae: 0.6493 - val_loss: 0.5297 - val_mae: 0.5707
    Epoch 346/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5761 - mae: 0.6107 - val_loss: 0.8996 - val_mae: 0.7427
    Epoch 347/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7056 - mae: 0.6439 - val_loss: 0.8437 - val_mae: 0.7450
    Epoch 348/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.6576 - mae: 0.5990 - val_loss: 0.6866 - val_mae: 0.6262
    Epoch 349/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.9373 - mae: 0.7702 - val_loss: 0.6834 - val_mae: 0.6368
    Epoch 350/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.7273 - mae: 0.6763 - val_loss: 0.6229 - val_mae: 0.5984
    Epoch 351/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.4951 - mae: 0.5964 - val_loss: 0.5560 - val_mae: 0.5815
    Epoch 352/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.6059 - mae: 0.6193 - val_loss: 0.7166 - val_mae: 0.6336
    Epoch 353/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6710 - mae: 0.6302 - val_loss: 0.6897 - val_mae: 0.6153
    Epoch 354/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5808 - mae: 0.5812 - val_loss: 0.6596 - val_mae: 0.6161
    Epoch 355/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.6535 - mae: 0.5973 - val_loss: 0.5715 - val_mae: 0.6069
    Epoch 356/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.4874 - mae: 0.5321 - val_loss: 0.9655 - val_mae: 0.7803
    Epoch 357/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6746 - mae: 0.6158 - val_loss: 0.9717 - val_mae: 0.7606
    Epoch 358/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4844 - mae: 0.5264 - val_loss: 0.7052 - val_mae: 0.6543
    Epoch 359/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.4755 - mae: 0.5624 - val_loss: 0.8098 - val_mae: 0.6732
    Epoch 360/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7218 - mae: 0.6450 - val_loss: 0.5631 - val_mae: 0.6113
    Epoch 361/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.5869 - mae: 0.5974 - val_loss: 0.5698 - val_mae: 0.6261
    Epoch 362/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6799 - mae: 0.6327 - val_loss: 0.5948 - val_mae: 0.6295
    Epoch 363/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5795 - mae: 0.6342 - val_loss: 0.7089 - val_mae: 0.6203
    Epoch 364/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5854 - mae: 0.6467 - val_loss: 0.6436 - val_mae: 0.6157
    Epoch 365/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5943 - mae: 0.6060 - val_loss: 0.7752 - val_mae: 0.6720
    Epoch 366/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.9657 - mae: 0.7581 - val_loss: 0.8278 - val_mae: 0.7165
    Epoch 367/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7433 - mae: 0.6640 - val_loss: 0.5320 - val_mae: 0.5625
    Epoch 368/1000
    26/26 [==============================] - 6s 177ms/step - loss: 0.7873 - mae: 0.6469 - val_loss: 0.8509 - val_mae: 0.6454
    Epoch 369/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.5277 - mae: 0.5732 - val_loss: 0.7646 - val_mae: 0.6750
    Epoch 370/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6418 - mae: 0.6046 - val_loss: 0.7643 - val_mae: 0.6627
    Epoch 371/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7275 - mae: 0.6380 - val_loss: 0.8352 - val_mae: 0.6737
    Epoch 372/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7930 - mae: 0.6894 - val_loss: 0.6391 - val_mae: 0.5811
    Epoch 373/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5197 - mae: 0.5662 - val_loss: 0.4193 - val_mae: 0.5170
    Epoch 374/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8866 - mae: 0.7334 - val_loss: 0.8196 - val_mae: 0.6999
    Epoch 375/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6524 - mae: 0.6482 - val_loss: 0.9490 - val_mae: 0.7681
    Epoch 376/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7142 - mae: 0.6502 - val_loss: 0.5194 - val_mae: 0.5535
    Epoch 377/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8059 - mae: 0.6840 - val_loss: 0.6550 - val_mae: 0.6124
    Epoch 378/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7467 - mae: 0.6419 - val_loss: 0.6032 - val_mae: 0.6528
    Epoch 379/1000
    26/26 [==============================] - 6s 178ms/step - loss: 0.6976 - mae: 0.6271 - val_loss: 0.6494 - val_mae: 0.5986
    Epoch 380/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.8969 - mae: 0.7275 - val_loss: 0.7753 - val_mae: 0.6618
    Epoch 381/1000
    26/26 [==============================] - 4s 116ms/step - loss: 1.0485 - mae: 0.7834 - val_loss: 0.5732 - val_mae: 0.5713
    Epoch 382/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.8900 - mae: 0.7037 - val_loss: 0.6787 - val_mae: 0.6684
    Epoch 383/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.8524 - mae: 0.7132 - val_loss: 0.4476 - val_mae: 0.5283
    Epoch 384/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8510 - mae: 0.6572 - val_loss: 0.7089 - val_mae: 0.6085
    Epoch 385/1000
    26/26 [==============================] - 5s 149ms/step - loss: 0.9510 - mae: 0.7334 - val_loss: 0.5451 - val_mae: 0.5897
    Epoch 386/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.7518 - mae: 0.6942 - val_loss: 0.6486 - val_mae: 0.6100
    Epoch 387/1000
    26/26 [==============================] - 3s 99ms/step - loss: 0.4974 - mae: 0.5310 - val_loss: 0.5134 - val_mae: 0.5471
    Epoch 388/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8555 - mae: 0.7405 - val_loss: 0.5697 - val_mae: 0.6069
    Epoch 389/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5632 - mae: 0.5973 - val_loss: 0.6980 - val_mae: 0.6467
    Epoch 390/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.4722 - mae: 0.4917 - val_loss: 0.8789 - val_mae: 0.6938
    Epoch 391/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7806 - mae: 0.6559 - val_loss: 0.5731 - val_mae: 0.5801
    Epoch 392/1000
    26/26 [==============================] - 5s 151ms/step - loss: 0.8946 - mae: 0.7271 - val_loss: 0.6709 - val_mae: 0.6639
    Epoch 393/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7027 - mae: 0.6281 - val_loss: 0.7745 - val_mae: 0.6983
    Epoch 394/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.6203 - mae: 0.6396 - val_loss: 0.4778 - val_mae: 0.5302
    Epoch 395/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8996 - mae: 0.7247 - val_loss: 0.7485 - val_mae: 0.6648
    Epoch 396/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5757 - mae: 0.5706 - val_loss: 0.6143 - val_mae: 0.6027
    Epoch 397/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8327 - mae: 0.7248 - val_loss: 0.6748 - val_mae: 0.6255
    Epoch 398/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6912 - mae: 0.6366 - val_loss: 0.6827 - val_mae: 0.6106
    Epoch 399/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8133 - mae: 0.6877 - val_loss: 0.8451 - val_mae: 0.7032
    Epoch 400/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.7243 - mae: 0.6672 - val_loss: 0.5720 - val_mae: 0.5903
    Epoch 401/1000
    26/26 [==============================] - 3s 99ms/step - loss: 0.7031 - mae: 0.6608 - val_loss: 0.8062 - val_mae: 0.6627
    Epoch 402/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7329 - mae: 0.6315 - val_loss: 0.8901 - val_mae: 0.6993
    Epoch 403/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.4809 - mae: 0.5481 - val_loss: 0.6691 - val_mae: 0.6419
    Epoch 404/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5827 - mae: 0.6106 - val_loss: 0.7760 - val_mae: 0.6814
    Epoch 405/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5699 - mae: 0.6046 - val_loss: 0.6908 - val_mae: 0.6567
    Epoch 406/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8775 - mae: 0.7046 - val_loss: 0.6790 - val_mae: 0.6367
    Epoch 407/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.7027 - mae: 0.6669 - val_loss: 0.7896 - val_mae: 0.6835
    Epoch 408/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.5215 - mae: 0.5507 - val_loss: 0.5771 - val_mae: 0.5625
    Epoch 409/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9814 - mae: 0.7697 - val_loss: 0.4722 - val_mae: 0.5667
    Epoch 410/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8104 - mae: 0.7016 - val_loss: 0.7480 - val_mae: 0.6995
    Epoch 411/1000
    26/26 [==============================] - 4s 125ms/step - loss: 1.0127 - mae: 0.7515 - val_loss: 0.9286 - val_mae: 0.7708
    Epoch 412/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6478 - mae: 0.6162 - val_loss: 0.9937 - val_mae: 0.7429
    Epoch 413/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8578 - mae: 0.7334 - val_loss: 0.6180 - val_mae: 0.6052
    Epoch 414/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8526 - mae: 0.6862 - val_loss: 0.7567 - val_mae: 0.6544
    Epoch 415/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7564 - mae: 0.6587 - val_loss: 0.5477 - val_mae: 0.5567
    Epoch 416/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5213 - mae: 0.5688 - val_loss: 1.0390 - val_mae: 0.7621
    Epoch 417/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4672 - mae: 0.5642 - val_loss: 0.7885 - val_mae: 0.6453
    Epoch 418/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7274 - mae: 0.6129 - val_loss: 0.9323 - val_mae: 0.7606
    Epoch 419/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6735 - mae: 0.5964 - val_loss: 0.7558 - val_mae: 0.6660
    Epoch 420/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6019 - mae: 0.5724 - val_loss: 0.6868 - val_mae: 0.6310
    Epoch 421/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8618 - mae: 0.7539 - val_loss: 0.6321 - val_mae: 0.5736
    Epoch 422/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5407 - mae: 0.5630 - val_loss: 0.7524 - val_mae: 0.6612
    Epoch 423/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7548 - mae: 0.7035 - val_loss: 0.6837 - val_mae: 0.6255
    Epoch 424/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.7727 - mae: 0.6937 - val_loss: 0.5519 - val_mae: 0.5654
    Epoch 425/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7236 - mae: 0.6808 - val_loss: 0.7561 - val_mae: 0.6698
    Epoch 426/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.7137 - mae: 0.6565 - val_loss: 0.5896 - val_mae: 0.5860
    Epoch 427/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5365 - mae: 0.5643 - val_loss: 0.8187 - val_mae: 0.6449
    Epoch 428/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7781 - mae: 0.6859 - val_loss: 0.6209 - val_mae: 0.5879
    Epoch 429/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9245 - mae: 0.7155 - val_loss: 0.5520 - val_mae: 0.5833
    Epoch 430/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7180 - mae: 0.6653 - val_loss: 0.7044 - val_mae: 0.6513
    Epoch 431/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7620 - mae: 0.6355 - val_loss: 0.7607 - val_mae: 0.6941
    Epoch 432/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.9533 - mae: 0.7208 - val_loss: 0.6613 - val_mae: 0.6091
    Epoch 433/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9152 - mae: 0.7337 - val_loss: 0.8299 - val_mae: 0.7247
    Epoch 434/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9312 - mae: 0.7130 - val_loss: 0.9585 - val_mae: 0.7076
    Epoch 435/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6813 - mae: 0.6184 - val_loss: 0.8404 - val_mae: 0.7103
    Epoch 436/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7544 - mae: 0.6823 - val_loss: 0.4254 - val_mae: 0.4970
    Epoch 437/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.5147 - mae: 0.5335 - val_loss: 0.8608 - val_mae: 0.6818
    Epoch 438/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5910 - mae: 0.6301 - val_loss: 0.8718 - val_mae: 0.7374
    Epoch 439/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6548 - mae: 0.6257 - val_loss: 0.5092 - val_mae: 0.5428
    Epoch 440/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5773 - mae: 0.5501 - val_loss: 0.6573 - val_mae: 0.6605
    Epoch 441/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7072 - mae: 0.6535 - val_loss: 0.7071 - val_mae: 0.6046
    Epoch 442/1000
    26/26 [==============================] - 3s 103ms/step - loss: 1.2981 - mae: 0.8547 - val_loss: 0.8052 - val_mae: 0.7193
    Epoch 443/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7124 - mae: 0.6445 - val_loss: 0.6758 - val_mae: 0.6380
    Epoch 444/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.5507 - mae: 0.6033 - val_loss: 0.7573 - val_mae: 0.6683
    Epoch 445/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6077 - mae: 0.6126 - val_loss: 0.6389 - val_mae: 0.6297
    Epoch 446/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6507 - mae: 0.6288 - val_loss: 0.6507 - val_mae: 0.6144
    Epoch 447/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.4740 - mae: 0.5535 - val_loss: 0.7125 - val_mae: 0.6928
    Epoch 448/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.5952 - mae: 0.5967 - val_loss: 0.5994 - val_mae: 0.6148
    Epoch 449/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6598 - mae: 0.6025 - val_loss: 0.8493 - val_mae: 0.6423
    Epoch 450/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.5870 - mae: 0.6255 - val_loss: 0.7873 - val_mae: 0.6777
    Epoch 451/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7344 - mae: 0.6350 - val_loss: 0.5987 - val_mae: 0.5875
    Epoch 452/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7274 - mae: 0.6528 - val_loss: 0.8600 - val_mae: 0.7052
    Epoch 453/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6186 - mae: 0.6115 - val_loss: 0.5745 - val_mae: 0.5916
    Epoch 454/1000
    26/26 [==============================] - 4s 120ms/step - loss: 0.6732 - mae: 0.6277 - val_loss: 0.7458 - val_mae: 0.6857
    Epoch 455/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7149 - mae: 0.6334 - val_loss: 0.7233 - val_mae: 0.6746
    Epoch 456/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.7512 - mae: 0.6775 - val_loss: 0.7489 - val_mae: 0.6125
    Epoch 457/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.7774 - mae: 0.6496 - val_loss: 0.6379 - val_mae: 0.6161
    Epoch 458/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7012 - mae: 0.6800 - val_loss: 0.6272 - val_mae: 0.5874
    Epoch 459/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5678 - mae: 0.5943 - val_loss: 0.6710 - val_mae: 0.6080
    Epoch 460/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7128 - mae: 0.6485 - val_loss: 0.7946 - val_mae: 0.6895
    Epoch 461/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7990 - mae: 0.6840 - val_loss: 0.8595 - val_mae: 0.7009
    Epoch 462/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7545 - mae: 0.6575 - val_loss: 0.7466 - val_mae: 0.6392
    Epoch 463/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8266 - mae: 0.6861 - val_loss: 0.8474 - val_mae: 0.7198
    Epoch 464/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7543 - mae: 0.6497 - val_loss: 0.7260 - val_mae: 0.6141
    Epoch 465/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7386 - mae: 0.6769 - val_loss: 0.6310 - val_mae: 0.6100
    Epoch 466/1000
    26/26 [==============================] - 4s 119ms/step - loss: 0.8179 - mae: 0.6884 - val_loss: 0.6602 - val_mae: 0.6188
    Epoch 467/1000
    26/26 [==============================] - 4s 124ms/step - loss: 1.1100 - mae: 0.8174 - val_loss: 0.7498 - val_mae: 0.6495
    Epoch 468/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.7915 - mae: 0.6643 - val_loss: 0.5781 - val_mae: 0.5874
    Epoch 469/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7192 - mae: 0.6235 - val_loss: 0.7519 - val_mae: 0.6217
    Epoch 470/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5487 - mae: 0.5831 - val_loss: 0.5496 - val_mae: 0.5780
    Epoch 471/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7094 - mae: 0.6333 - val_loss: 0.7477 - val_mae: 0.6435
    Epoch 472/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7251 - mae: 0.6450 - val_loss: 0.6011 - val_mae: 0.6382
    Epoch 473/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6535 - mae: 0.6138 - val_loss: 0.7442 - val_mae: 0.6865
    Epoch 474/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5578 - mae: 0.5651 - val_loss: 0.8787 - val_mae: 0.6841
    Epoch 475/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7463 - mae: 0.6478 - val_loss: 0.5254 - val_mae: 0.5871
    Epoch 476/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6296 - mae: 0.6227 - val_loss: 0.6319 - val_mae: 0.5913
    Epoch 477/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7931 - mae: 0.6300 - val_loss: 1.0938 - val_mae: 0.8071
    Epoch 478/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6213 - mae: 0.5896 - val_loss: 0.5611 - val_mae: 0.6346
    Epoch 479/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5096 - mae: 0.5581 - val_loss: 0.7560 - val_mae: 0.6611
    Epoch 480/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5457 - mae: 0.5583 - val_loss: 0.7024 - val_mae: 0.6860
    Epoch 481/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7307 - mae: 0.6645 - val_loss: 0.8685 - val_mae: 0.6750
    Epoch 482/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6293 - mae: 0.5862 - val_loss: 0.8749 - val_mae: 0.7210
    Epoch 483/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.7308 - mae: 0.6922 - val_loss: 0.6987 - val_mae: 0.6016
    Epoch 484/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.4775 - mae: 0.5479 - val_loss: 0.5266 - val_mae: 0.5892
    Epoch 485/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7267 - mae: 0.6545 - val_loss: 0.4858 - val_mae: 0.5863
    Epoch 486/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6402 - mae: 0.6192 - val_loss: 0.7520 - val_mae: 0.6710
    Epoch 487/1000
    26/26 [==============================] - 4s 120ms/step - loss: 0.9795 - mae: 0.7067 - val_loss: 0.6435 - val_mae: 0.6446
    Epoch 488/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5391 - mae: 0.6069 - val_loss: 0.6849 - val_mae: 0.6560
    Epoch 489/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8459 - mae: 0.7108 - val_loss: 0.8033 - val_mae: 0.6734
    Epoch 490/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7424 - mae: 0.6403 - val_loss: 0.7078 - val_mae: 0.6437
    Epoch 491/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7264 - mae: 0.6880 - val_loss: 0.7511 - val_mae: 0.6244
    Epoch 492/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.7897 - mae: 0.6664 - val_loss: 0.7212 - val_mae: 0.6336
    Epoch 493/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.4976 - mae: 0.5670 - val_loss: 0.7685 - val_mae: 0.6479
    Epoch 494/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.6839 - mae: 0.6665 - val_loss: 0.7508 - val_mae: 0.6131
    Epoch 495/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5357 - mae: 0.5474 - val_loss: 0.6827 - val_mae: 0.6146
    Epoch 496/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8455 - mae: 0.7189 - val_loss: 0.5586 - val_mae: 0.5699
    Epoch 497/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.4923 - mae: 0.5292 - val_loss: 1.0661 - val_mae: 0.7699
    Epoch 498/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5898 - mae: 0.5878 - val_loss: 0.6027 - val_mae: 0.6303
    Epoch 499/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6349 - mae: 0.6193 - val_loss: 0.5435 - val_mae: 0.6074
    Epoch 500/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9728 - mae: 0.7712 - val_loss: 0.7363 - val_mae: 0.6949
    Epoch 501/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8806 - mae: 0.7055 - val_loss: 0.6975 - val_mae: 0.6408
    Epoch 502/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6765 - mae: 0.6078 - val_loss: 0.5361 - val_mae: 0.6026
    Epoch 503/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.9378 - mae: 0.7502 - val_loss: 0.8689 - val_mae: 0.7383
    Epoch 504/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5694 - mae: 0.5751 - val_loss: 0.9078 - val_mae: 0.7320
    Epoch 505/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5850 - mae: 0.5840 - val_loss: 0.6488 - val_mae: 0.5949
    Epoch 506/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5626 - mae: 0.5648 - val_loss: 0.8338 - val_mae: 0.6719
    Epoch 507/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5053 - mae: 0.5057 - val_loss: 0.8156 - val_mae: 0.7071
    Epoch 508/1000
    26/26 [==============================] - 4s 127ms/step - loss: 1.0423 - mae: 0.7869 - val_loss: 0.8569 - val_mae: 0.7550
    Epoch 509/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6954 - mae: 0.6158 - val_loss: 0.7376 - val_mae: 0.6348
    Epoch 510/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.5037 - mae: 0.5305 - val_loss: 0.9717 - val_mae: 0.7510
    Epoch 511/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5750 - mae: 0.6164 - val_loss: 0.7201 - val_mae: 0.6758
    Epoch 512/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5461 - mae: 0.5576 - val_loss: 0.5411 - val_mae: 0.5735
    Epoch 513/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.9332 - mae: 0.7220 - val_loss: 0.4866 - val_mae: 0.5015
    Epoch 514/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6450 - mae: 0.6592 - val_loss: 0.6341 - val_mae: 0.6631
    Epoch 515/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5507 - mae: 0.5929 - val_loss: 0.8842 - val_mae: 0.7464
    Epoch 516/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6356 - mae: 0.6238 - val_loss: 0.7398 - val_mae: 0.6556
    Epoch 517/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6639 - mae: 0.5833 - val_loss: 0.8069 - val_mae: 0.6960
    Epoch 518/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7311 - mae: 0.6659 - val_loss: 0.5761 - val_mae: 0.5681
    Epoch 519/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.6056 - mae: 0.6377 - val_loss: 0.7100 - val_mae: 0.6211
    Epoch 520/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.7181 - mae: 0.6506 - val_loss: 0.5612 - val_mae: 0.5947
    Epoch 521/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.6091 - mae: 0.5912 - val_loss: 0.7354 - val_mae: 0.6354
    Epoch 522/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7794 - mae: 0.6747 - val_loss: 0.8332 - val_mae: 0.6963
    Epoch 523/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7352 - mae: 0.6804 - val_loss: 0.6579 - val_mae: 0.5951
    Epoch 524/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.9088 - mae: 0.7218 - val_loss: 0.8792 - val_mae: 0.7075
    Epoch 525/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6733 - mae: 0.6123 - val_loss: 0.7234 - val_mae: 0.6276
    Epoch 526/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5860 - mae: 0.5792 - val_loss: 0.8288 - val_mae: 0.6766
    Epoch 527/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7976 - mae: 0.6830 - val_loss: 0.9736 - val_mae: 0.7035
    Epoch 528/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6554 - mae: 0.6068 - val_loss: 0.9668 - val_mae: 0.7299
    Epoch 529/1000
    26/26 [==============================] - 4s 129ms/step - loss: 0.5364 - mae: 0.5524 - val_loss: 0.8350 - val_mae: 0.6861
    Epoch 530/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.9014 - mae: 0.6960 - val_loss: 0.6165 - val_mae: 0.6143
    Epoch 531/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.8029 - mae: 0.7028 - val_loss: 0.5278 - val_mae: 0.5608
    Epoch 532/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6994 - mae: 0.6245 - val_loss: 0.5699 - val_mae: 0.5636
    Epoch 533/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7086 - mae: 0.6657 - val_loss: 1.0117 - val_mae: 0.7138
    Epoch 534/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7634 - mae: 0.6655 - val_loss: 0.5519 - val_mae: 0.5668
    Epoch 535/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8057 - mae: 0.6946 - val_loss: 0.7314 - val_mae: 0.6330
    Epoch 536/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7853 - mae: 0.6680 - val_loss: 0.7968 - val_mae: 0.6974
    Epoch 537/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6650 - mae: 0.6287 - val_loss: 0.7344 - val_mae: 0.6832
    Epoch 538/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.7915 - mae: 0.6645 - val_loss: 0.7935 - val_mae: 0.6727
    Epoch 539/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6138 - mae: 0.6120 - val_loss: 0.6591 - val_mae: 0.6277
    Epoch 540/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.9009 - mae: 0.7335 - val_loss: 0.7570 - val_mae: 0.6555
    Epoch 541/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8383 - mae: 0.7036 - val_loss: 0.5461 - val_mae: 0.5429
    Epoch 542/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7235 - mae: 0.6412 - val_loss: 0.7617 - val_mae: 0.6886
    Epoch 543/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5459 - mae: 0.5789 - val_loss: 0.5897 - val_mae: 0.5901
    Epoch 544/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5772 - mae: 0.5789 - val_loss: 1.0785 - val_mae: 0.7817
    Epoch 545/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.8998 - mae: 0.7315 - val_loss: 0.7487 - val_mae: 0.6796
    Epoch 546/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8330 - mae: 0.7038 - val_loss: 0.8162 - val_mae: 0.6581
    Epoch 547/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6522 - mae: 0.6246 - val_loss: 0.7134 - val_mae: 0.6163
    Epoch 548/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.7492 - mae: 0.6641 - val_loss: 0.5137 - val_mae: 0.5464
    Epoch 549/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5258 - mae: 0.5614 - val_loss: 1.0461 - val_mae: 0.7999
    Epoch 550/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6233 - mae: 0.6105 - val_loss: 0.4642 - val_mae: 0.5210
    Epoch 551/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6544 - mae: 0.5959 - val_loss: 0.5594 - val_mae: 0.5632
    Epoch 552/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.8494 - mae: 0.7349 - val_loss: 0.6911 - val_mae: 0.6656
    Epoch 553/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8619 - mae: 0.6973 - val_loss: 0.5683 - val_mae: 0.6211
    Epoch 554/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7061 - mae: 0.6469 - val_loss: 0.8115 - val_mae: 0.7295
    Epoch 555/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5716 - mae: 0.5811 - val_loss: 0.5471 - val_mae: 0.5932
    Epoch 556/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8284 - mae: 0.6937 - val_loss: 0.8118 - val_mae: 0.6933
    Epoch 557/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.4629 - mae: 0.5210 - val_loss: 0.6218 - val_mae: 0.5494
    Epoch 558/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5087 - mae: 0.5309 - val_loss: 0.8987 - val_mae: 0.7491
    Epoch 559/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7521 - mae: 0.6255 - val_loss: 0.7621 - val_mae: 0.6684
    Epoch 560/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7937 - mae: 0.6816 - val_loss: 0.7920 - val_mae: 0.6463
    Epoch 561/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.9239 - mae: 0.7173 - val_loss: 1.0418 - val_mae: 0.7660
    Epoch 562/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5392 - mae: 0.5348 - val_loss: 0.5213 - val_mae: 0.6083
    Epoch 563/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5959 - mae: 0.5900 - val_loss: 0.4237 - val_mae: 0.5338
    Epoch 564/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6389 - mae: 0.6308 - val_loss: 0.6242 - val_mae: 0.6004
    Epoch 565/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.9145 - mae: 0.7289 - val_loss: 0.8070 - val_mae: 0.6921
    Epoch 566/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5895 - mae: 0.5917 - val_loss: 0.9549 - val_mae: 0.7283
    Epoch 567/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.4017 - mae: 0.5304 - val_loss: 0.7375 - val_mae: 0.6715
    Epoch 568/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6842 - mae: 0.6370 - val_loss: 1.0281 - val_mae: 0.7944
    Epoch 569/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.6653 - mae: 0.5920 - val_loss: 0.7257 - val_mae: 0.6698
    Epoch 570/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6766 - mae: 0.6581 - val_loss: 0.6545 - val_mae: 0.6220
    Epoch 571/1000
    26/26 [==============================] - 4s 110ms/step - loss: 0.8957 - mae: 0.7075 - val_loss: 0.8488 - val_mae: 0.6811
    Epoch 572/1000
    26/26 [==============================] - 3s 103ms/step - loss: 1.1828 - mae: 0.8515 - val_loss: 0.9075 - val_mae: 0.7496
    Epoch 573/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.8797 - mae: 0.7112 - val_loss: 0.5767 - val_mae: 0.6064
    Epoch 574/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5139 - mae: 0.5351 - val_loss: 0.5627 - val_mae: 0.5878
    Epoch 575/1000
    26/26 [==============================] - 4s 110ms/step - loss: 0.7085 - mae: 0.6506 - val_loss: 0.7424 - val_mae: 0.6988
    Epoch 576/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5856 - mae: 0.6227 - val_loss: 0.7084 - val_mae: 0.6537
    Epoch 577/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.7848 - mae: 0.6685 - val_loss: 0.9359 - val_mae: 0.7378
    Epoch 578/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6615 - mae: 0.5878 - val_loss: 0.4683 - val_mae: 0.5057
    Epoch 579/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6553 - mae: 0.6117 - val_loss: 0.7942 - val_mae: 0.7241
    Epoch 580/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.5240 - mae: 0.5604 - val_loss: 0.8970 - val_mae: 0.7515
    Epoch 581/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.9150 - mae: 0.6926 - val_loss: 0.8389 - val_mae: 0.6687
    Epoch 582/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6964 - mae: 0.6329 - val_loss: 0.6469 - val_mae: 0.5743
    Epoch 583/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7591 - mae: 0.6853 - val_loss: 0.7571 - val_mae: 0.6630
    Epoch 584/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8045 - mae: 0.7004 - val_loss: 0.9205 - val_mae: 0.6816
    Epoch 585/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5867 - mae: 0.6104 - val_loss: 0.7898 - val_mae: 0.6620
    Epoch 586/1000
    26/26 [==============================] - 5s 152ms/step - loss: 0.6267 - mae: 0.5818 - val_loss: 0.6866 - val_mae: 0.6392
    Epoch 587/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6731 - mae: 0.6263 - val_loss: 0.5124 - val_mae: 0.5509
    Epoch 588/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6416 - mae: 0.5978 - val_loss: 0.7356 - val_mae: 0.6354
    Epoch 589/1000
    26/26 [==============================] - 4s 124ms/step - loss: 1.0101 - mae: 0.7867 - val_loss: 1.0286 - val_mae: 0.7337
    Epoch 590/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6836 - mae: 0.6288 - val_loss: 0.6533 - val_mae: 0.5959
    Epoch 591/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5591 - mae: 0.5712 - val_loss: 0.5060 - val_mae: 0.5356
    Epoch 592/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6924 - mae: 0.6503 - val_loss: 0.7729 - val_mae: 0.6811
    Epoch 593/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7406 - mae: 0.6310 - val_loss: 0.6824 - val_mae: 0.6186
    Epoch 594/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6899 - mae: 0.6377 - val_loss: 0.9457 - val_mae: 0.7584
    Epoch 595/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5592 - mae: 0.5730 - val_loss: 0.7883 - val_mae: 0.6492
    Epoch 596/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.7029 - mae: 0.6107 - val_loss: 0.4749 - val_mae: 0.5062
    Epoch 597/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.9722 - mae: 0.7237 - val_loss: 0.9669 - val_mae: 0.7568
    Epoch 598/1000
    26/26 [==============================] - 5s 171ms/step - loss: 0.8603 - mae: 0.6932 - val_loss: 0.8922 - val_mae: 0.7237
    Epoch 599/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.6201 - mae: 0.5930 - val_loss: 0.3385 - val_mae: 0.4701
    Epoch 600/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7385 - mae: 0.6686 - val_loss: 0.6380 - val_mae: 0.6056
    Epoch 601/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.5020 - mae: 0.5580 - val_loss: 0.7033 - val_mae: 0.6430
    Epoch 602/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8202 - mae: 0.7323 - val_loss: 0.8111 - val_mae: 0.6636
    Epoch 603/1000
    26/26 [==============================] - 6s 172ms/step - loss: 0.8548 - mae: 0.6801 - val_loss: 0.7235 - val_mae: 0.6600
    Epoch 604/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.5062 - mae: 0.5428 - val_loss: 0.8504 - val_mae: 0.6979
    Epoch 605/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5161 - mae: 0.5881 - val_loss: 0.7122 - val_mae: 0.6979
    Epoch 606/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8801 - mae: 0.6834 - val_loss: 0.8064 - val_mae: 0.6663
    Epoch 607/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.9447 - mae: 0.7318 - val_loss: 0.7421 - val_mae: 0.6367
    Epoch 608/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9271 - mae: 0.6847 - val_loss: 0.9579 - val_mae: 0.8066
    Epoch 609/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6613 - mae: 0.6351 - val_loss: 0.6894 - val_mae: 0.6411
    Epoch 610/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7934 - mae: 0.7428 - val_loss: 0.7157 - val_mae: 0.6410
    Epoch 611/1000
    26/26 [==============================] - 3s 104ms/step - loss: 0.5766 - mae: 0.6425 - val_loss: 0.9083 - val_mae: 0.7274
    Epoch 612/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.4309 - mae: 0.4872 - val_loss: 0.5350 - val_mae: 0.5111
    Epoch 613/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6713 - mae: 0.5782 - val_loss: 0.5535 - val_mae: 0.5570
    Epoch 614/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8728 - mae: 0.7308 - val_loss: 0.6048 - val_mae: 0.5884
    Epoch 615/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8383 - mae: 0.7687 - val_loss: 0.5728 - val_mae: 0.6029
    Epoch 616/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6051 - mae: 0.5740 - val_loss: 0.8211 - val_mae: 0.6979
    Epoch 617/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.8243 - mae: 0.6756 - val_loss: 0.6083 - val_mae: 0.5937
    Epoch 618/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.5270 - mae: 0.5359 - val_loss: 0.5885 - val_mae: 0.5697
    Epoch 619/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.4652 - mae: 0.5231 - val_loss: 0.6577 - val_mae: 0.6606
    Epoch 620/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.6302 - mae: 0.6008 - val_loss: 0.6834 - val_mae: 0.6461
    Epoch 621/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7949 - mae: 0.6547 - val_loss: 0.6861 - val_mae: 0.6184
    Epoch 622/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7353 - mae: 0.6405 - val_loss: 0.6967 - val_mae: 0.6403
    Epoch 623/1000
    26/26 [==============================] - 6s 181ms/step - loss: 0.6547 - mae: 0.6126 - val_loss: 0.5933 - val_mae: 0.6131
    Epoch 624/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.4633 - mae: 0.5541 - val_loss: 0.7291 - val_mae: 0.6766
    Epoch 625/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6361 - mae: 0.5855 - val_loss: 0.7413 - val_mae: 0.6452
    Epoch 626/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5144 - mae: 0.5382 - val_loss: 0.7690 - val_mae: 0.6529
    Epoch 627/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7113 - mae: 0.6266 - val_loss: 0.8734 - val_mae: 0.7288
    Epoch 628/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6256 - mae: 0.5769 - val_loss: 0.6573 - val_mae: 0.6124
    Epoch 629/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.9110 - mae: 0.7496 - val_loss: 0.3822 - val_mae: 0.4849
    Epoch 630/1000
    26/26 [==============================] - 5s 149ms/step - loss: 0.7280 - mae: 0.6580 - val_loss: 0.5074 - val_mae: 0.5316
    Epoch 631/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.4966 - mae: 0.5290 - val_loss: 0.4894 - val_mae: 0.5384
    Epoch 632/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.9270 - mae: 0.7097 - val_loss: 0.7728 - val_mae: 0.6716
    Epoch 633/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6261 - mae: 0.6072 - val_loss: 0.7483 - val_mae: 0.6553
    Epoch 634/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.6170 - mae: 0.6164 - val_loss: 0.5356 - val_mae: 0.5999
    Epoch 635/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6401 - mae: 0.6720 - val_loss: 0.8131 - val_mae: 0.6790
    Epoch 636/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6548 - mae: 0.6300 - val_loss: 0.6770 - val_mae: 0.6525
    Epoch 637/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.4681 - mae: 0.5470 - val_loss: 0.6874 - val_mae: 0.6352
    Epoch 638/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7217 - mae: 0.6415 - val_loss: 0.4763 - val_mae: 0.5320
    Epoch 639/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8799 - mae: 0.7205 - val_loss: 1.0726 - val_mae: 0.7881
    Epoch 640/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7088 - mae: 0.6548 - val_loss: 0.4529 - val_mae: 0.5039
    Epoch 641/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5739 - mae: 0.5655 - val_loss: 0.7327 - val_mae: 0.6553
    Epoch 642/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7551 - mae: 0.6434 - val_loss: 0.9380 - val_mae: 0.7077
    Epoch 643/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5205 - mae: 0.5571 - val_loss: 0.6856 - val_mae: 0.6109
    Epoch 644/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7598 - mae: 0.6590 - val_loss: 0.9385 - val_mae: 0.7179
    Epoch 645/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6505 - mae: 0.6009 - val_loss: 0.5818 - val_mae: 0.5432
    Epoch 646/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8014 - mae: 0.7323 - val_loss: 0.5897 - val_mae: 0.5818
    Epoch 647/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7317 - mae: 0.6598 - val_loss: 0.8647 - val_mae: 0.7507
    Epoch 648/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7826 - mae: 0.6763 - val_loss: 0.7897 - val_mae: 0.7442
    Epoch 649/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8585 - mae: 0.7174 - val_loss: 0.9618 - val_mae: 0.7323
    Epoch 650/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.8476 - mae: 0.7052 - val_loss: 0.7726 - val_mae: 0.6475
    Epoch 651/1000
    26/26 [==============================] - 6s 177ms/step - loss: 0.7916 - mae: 0.6859 - val_loss: 0.7426 - val_mae: 0.6765
    Epoch 652/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.8889 - mae: 0.7306 - val_loss: 0.9557 - val_mae: 0.7303
    Epoch 653/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.8163 - mae: 0.6653 - val_loss: 0.6445 - val_mae: 0.6104
    Epoch 654/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5873 - mae: 0.5654 - val_loss: 0.8755 - val_mae: 0.7000
    Epoch 655/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8699 - mae: 0.6861 - val_loss: 0.5635 - val_mae: 0.5492
    Epoch 656/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7209 - mae: 0.6266 - val_loss: 0.9466 - val_mae: 0.7588
    Epoch 657/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8268 - mae: 0.6989 - val_loss: 0.5471 - val_mae: 0.5675
    Epoch 658/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.7524 - mae: 0.6474 - val_loss: 0.8970 - val_mae: 0.7168
    Epoch 659/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6074 - mae: 0.6304 - val_loss: 0.6243 - val_mae: 0.6339
    Epoch 660/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7899 - mae: 0.6579 - val_loss: 0.7507 - val_mae: 0.6472
    Epoch 661/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7805 - mae: 0.6570 - val_loss: 0.7320 - val_mae: 0.6358
    Epoch 662/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.7602 - mae: 0.6633 - val_loss: 0.7443 - val_mae: 0.6922
    Epoch 663/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6085 - mae: 0.5670 - val_loss: 0.6529 - val_mae: 0.5947
    Epoch 664/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6630 - mae: 0.6243 - val_loss: 0.6140 - val_mae: 0.5897
    Epoch 665/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9325 - mae: 0.6935 - val_loss: 0.5098 - val_mae: 0.5538
    Epoch 666/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6267 - mae: 0.6433 - val_loss: 1.0983 - val_mae: 0.8166
    Epoch 667/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5922 - mae: 0.5507 - val_loss: 0.5546 - val_mae: 0.5699
    Epoch 668/1000
    26/26 [==============================] - 3s 104ms/step - loss: 0.5110 - mae: 0.5594 - val_loss: 0.9556 - val_mae: 0.7211
    Epoch 669/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9534 - mae: 0.7847 - val_loss: 0.9146 - val_mae: 0.7115
    Epoch 670/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7502 - mae: 0.7070 - val_loss: 0.8322 - val_mae: 0.7102
    Epoch 671/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.6533 - mae: 0.6127 - val_loss: 0.6882 - val_mae: 0.6716
    Epoch 672/1000
    26/26 [==============================] - 5s 170ms/step - loss: 0.6183 - mae: 0.6139 - val_loss: 0.7493 - val_mae: 0.6160
    Epoch 673/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7738 - mae: 0.6662 - val_loss: 0.7534 - val_mae: 0.6404
    Epoch 674/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7573 - mae: 0.6761 - val_loss: 0.5511 - val_mae: 0.5619
    Epoch 675/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9796 - mae: 0.7654 - val_loss: 0.3567 - val_mae: 0.4692
    Epoch 676/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5941 - mae: 0.5767 - val_loss: 0.7936 - val_mae: 0.7007
    Epoch 677/1000
    26/26 [==============================] - 4s 100ms/step - loss: 0.6899 - mae: 0.6420 - val_loss: 0.5329 - val_mae: 0.5451
    Epoch 678/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6220 - mae: 0.6110 - val_loss: 0.6316 - val_mae: 0.6077
    Epoch 679/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8564 - mae: 0.7416 - val_loss: 0.6326 - val_mae: 0.5808
    Epoch 680/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.5928 - mae: 0.6029 - val_loss: 0.8327 - val_mae: 0.6892
    Epoch 681/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5905 - mae: 0.5409 - val_loss: 0.7168 - val_mae: 0.6233
    Epoch 682/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.6682 - mae: 0.6561 - val_loss: 0.5808 - val_mae: 0.5617
    Epoch 683/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7302 - mae: 0.6144 - val_loss: 0.8323 - val_mae: 0.7076
    Epoch 684/1000
    26/26 [==============================] - 4s 111ms/step - loss: 0.9079 - mae: 0.7157 - val_loss: 0.8111 - val_mae: 0.7077
    Epoch 685/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.8627 - mae: 0.7031 - val_loss: 0.7760 - val_mae: 0.6194
    Epoch 686/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8170 - mae: 0.7038 - val_loss: 0.8377 - val_mae: 0.7181
    Epoch 687/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8401 - mae: 0.6746 - val_loss: 0.5791 - val_mae: 0.5693
    Epoch 688/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8751 - mae: 0.7648 - val_loss: 0.4225 - val_mae: 0.4972
    Epoch 689/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6583 - mae: 0.6450 - val_loss: 0.6523 - val_mae: 0.6200
    Epoch 690/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.4789 - mae: 0.5386 - val_loss: 0.7724 - val_mae: 0.6418
    Epoch 691/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.9777 - mae: 0.7619 - val_loss: 0.9066 - val_mae: 0.7248
    Epoch 692/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6937 - mae: 0.6119 - val_loss: 0.9177 - val_mae: 0.7724
    Epoch 693/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.4172 - mae: 0.5042 - val_loss: 1.1362 - val_mae: 0.8071
    Epoch 694/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7839 - mae: 0.6665 - val_loss: 0.8644 - val_mae: 0.7507
    Epoch 695/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9192 - mae: 0.7738 - val_loss: 0.7419 - val_mae: 0.6411
    Epoch 696/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.5868 - mae: 0.5893 - val_loss: 0.4800 - val_mae: 0.5203
    Epoch 697/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7741 - mae: 0.6623 - val_loss: 0.7302 - val_mae: 0.6002
    Epoch 698/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.4454 - mae: 0.5073 - val_loss: 0.4696 - val_mae: 0.5609
    Epoch 699/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6793 - mae: 0.6160 - val_loss: 0.5191 - val_mae: 0.5572
    Epoch 700/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5400 - mae: 0.5470 - val_loss: 0.5470 - val_mae: 0.5759
    Epoch 701/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6558 - mae: 0.6083 - val_loss: 0.6852 - val_mae: 0.6584
    Epoch 702/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.8486 - mae: 0.6820 - val_loss: 0.6068 - val_mae: 0.5909
    Epoch 703/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.6538 - mae: 0.6152 - val_loss: 0.4723 - val_mae: 0.5230
    Epoch 704/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8211 - mae: 0.6735 - val_loss: 0.8420 - val_mae: 0.7118
    Epoch 705/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8266 - mae: 0.7230 - val_loss: 0.8313 - val_mae: 0.6468
    Epoch 706/1000
    26/26 [==============================] - 5s 154ms/step - loss: 0.7265 - mae: 0.6677 - val_loss: 0.6042 - val_mae: 0.6003
    Epoch 707/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9002 - mae: 0.7834 - val_loss: 0.6063 - val_mae: 0.5644
    Epoch 708/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6777 - mae: 0.6096 - val_loss: 0.6817 - val_mae: 0.6442
    Epoch 709/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8318 - mae: 0.6668 - val_loss: 0.6912 - val_mae: 0.6419
    Epoch 710/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6279 - mae: 0.5923 - val_loss: 0.9339 - val_mae: 0.7210
    Epoch 711/1000
    26/26 [==============================] - 4s 101ms/step - loss: 1.1989 - mae: 0.8689 - val_loss: 0.5403 - val_mae: 0.5814
    Epoch 712/1000
    26/26 [==============================] - 4s 112ms/step - loss: 0.7273 - mae: 0.6699 - val_loss: 0.9338 - val_mae: 0.7325
    Epoch 713/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8977 - mae: 0.7134 - val_loss: 0.5111 - val_mae: 0.5495
    Epoch 714/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.7936 - mae: 0.7631 - val_loss: 0.6556 - val_mae: 0.6104
    Epoch 715/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6539 - mae: 0.6424 - val_loss: 0.5254 - val_mae: 0.5393
    Epoch 716/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5780 - mae: 0.5544 - val_loss: 0.8812 - val_mae: 0.6822
    Epoch 717/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.8366 - mae: 0.6881 - val_loss: 0.6050 - val_mae: 0.5869
    Epoch 718/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.5965 - mae: 0.5951 - val_loss: 0.6496 - val_mae: 0.6254
    Epoch 719/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5311 - mae: 0.5481 - val_loss: 0.9805 - val_mae: 0.7704
    Epoch 720/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.2516 - mae: 0.8476 - val_loss: 0.7183 - val_mae: 0.6350
    Epoch 721/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.4140 - mae: 0.4968 - val_loss: 0.7756 - val_mae: 0.7039
    Epoch 722/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8105 - mae: 0.6460 - val_loss: 0.7255 - val_mae: 0.6292
    Epoch 723/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.8036 - mae: 0.6870 - val_loss: 0.7497 - val_mae: 0.6680
    Epoch 724/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.5791 - mae: 0.6020 - val_loss: 0.5357 - val_mae: 0.5675
    Epoch 725/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.9987 - mae: 0.8231 - val_loss: 0.7794 - val_mae: 0.6998
    Epoch 726/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.9016 - mae: 0.7266 - val_loss: 1.1478 - val_mae: 0.8526
    Epoch 727/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5730 - mae: 0.5716 - val_loss: 0.7841 - val_mae: 0.6749
    Epoch 728/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6686 - mae: 0.6420 - val_loss: 0.4563 - val_mae: 0.5088
    Epoch 729/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.4446 - mae: 0.5180 - val_loss: 0.6742 - val_mae: 0.6289
    Epoch 730/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.9667 - mae: 0.7446 - val_loss: 0.8775 - val_mae: 0.7269
    Epoch 731/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.5560 - mae: 0.5875 - val_loss: 0.9768 - val_mae: 0.7536
    Epoch 732/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6066 - mae: 0.6062 - val_loss: 0.8031 - val_mae: 0.6793
    Epoch 733/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.8269 - mae: 0.6818 - val_loss: 0.6212 - val_mae: 0.6034
    Epoch 734/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6079 - mae: 0.6305 - val_loss: 0.8296 - val_mae: 0.7153
    Epoch 735/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8532 - mae: 0.6734 - val_loss: 0.6719 - val_mae: 0.6262
    Epoch 736/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8028 - mae: 0.6791 - val_loss: 0.7050 - val_mae: 0.6171
    Epoch 737/1000
    26/26 [==============================] - 4s 110ms/step - loss: 0.7835 - mae: 0.6757 - val_loss: 0.5794 - val_mae: 0.5947
    Epoch 738/1000
    26/26 [==============================] - 5s 159ms/step - loss: 0.5719 - mae: 0.5852 - val_loss: 0.8610 - val_mae: 0.7202
    Epoch 739/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5215 - mae: 0.5612 - val_loss: 0.7855 - val_mae: 0.6486
    Epoch 740/1000
    26/26 [==============================] - 4s 130ms/step - loss: 0.5634 - mae: 0.5935 - val_loss: 0.5644 - val_mae: 0.5618
    Epoch 741/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5405 - mae: 0.5837 - val_loss: 0.6107 - val_mae: 0.6018
    Epoch 742/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8580 - mae: 0.7084 - val_loss: 0.8852 - val_mae: 0.7076
    Epoch 743/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.6550 - mae: 0.6099 - val_loss: 0.7675 - val_mae: 0.6569
    Epoch 744/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6443 - mae: 0.5971 - val_loss: 0.8406 - val_mae: 0.6864
    Epoch 745/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8792 - mae: 0.7312 - val_loss: 0.4718 - val_mae: 0.5035
    Epoch 746/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5620 - mae: 0.5826 - val_loss: 0.6057 - val_mae: 0.5997
    Epoch 747/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7494 - mae: 0.6831 - val_loss: 0.6596 - val_mae: 0.5772
    Epoch 748/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.4895 - mae: 0.5559 - val_loss: 0.6154 - val_mae: 0.5757
    Epoch 749/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.6716 - mae: 0.6209 - val_loss: 0.6579 - val_mae: 0.6257
    Epoch 750/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7008 - mae: 0.6091 - val_loss: 0.9041 - val_mae: 0.7627
    Epoch 751/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5411 - mae: 0.5824 - val_loss: 0.7521 - val_mae: 0.6580
    Epoch 752/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8022 - mae: 0.6632 - val_loss: 0.7180 - val_mae: 0.6287
    Epoch 753/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6971 - mae: 0.6246 - val_loss: 0.6620 - val_mae: 0.6248
    Epoch 754/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6534 - mae: 0.6194 - val_loss: 0.5384 - val_mae: 0.5682
    Epoch 755/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8795 - mae: 0.7017 - val_loss: 0.9060 - val_mae: 0.7162
    Epoch 756/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6390 - mae: 0.6079 - val_loss: 1.0809 - val_mae: 0.7751
    Epoch 757/1000
    26/26 [==============================] - 4s 122ms/step - loss: 1.1892 - mae: 0.8192 - val_loss: 0.7006 - val_mae: 0.6291
    Epoch 758/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7285 - mae: 0.6671 - val_loss: 0.6561 - val_mae: 0.5980
    Epoch 759/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.7013 - mae: 0.6334 - val_loss: 0.6234 - val_mae: 0.6461
    Epoch 760/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7394 - mae: 0.6906 - val_loss: 0.4523 - val_mae: 0.4842
    Epoch 761/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7780 - mae: 0.6702 - val_loss: 0.7143 - val_mae: 0.6717
    Epoch 762/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7786 - mae: 0.6649 - val_loss: 0.7661 - val_mae: 0.6593
    Epoch 763/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8486 - mae: 0.7020 - val_loss: 0.5199 - val_mae: 0.5128
    Epoch 764/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7037 - mae: 0.6623 - val_loss: 0.7814 - val_mae: 0.6540
    Epoch 765/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.7949 - mae: 0.6608 - val_loss: 0.6407 - val_mae: 0.6045
    Epoch 766/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.5709 - mae: 0.5413 - val_loss: 0.8740 - val_mae: 0.7419
    Epoch 767/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.5302 - mae: 0.5485 - val_loss: 0.5838 - val_mae: 0.6058
    Epoch 768/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7891 - mae: 0.6826 - val_loss: 0.9311 - val_mae: 0.7515
    Epoch 769/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.5503 - mae: 0.5531 - val_loss: 1.0403 - val_mae: 0.7713
    Epoch 770/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.5036 - mae: 0.5310 - val_loss: 0.6856 - val_mae: 0.6262
    Epoch 771/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.4889 - mae: 0.5504 - val_loss: 0.7426 - val_mae: 0.6754
    Epoch 772/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.5400 - mae: 0.5650 - val_loss: 0.7242 - val_mae: 0.6936
    Epoch 773/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.8970 - mae: 0.7170 - val_loss: 1.0093 - val_mae: 0.7371
    Epoch 774/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.9272 - mae: 0.7364 - val_loss: 0.6932 - val_mae: 0.6625
    Epoch 775/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8748 - mae: 0.7192 - val_loss: 0.8042 - val_mae: 0.6544
    Epoch 776/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6772 - mae: 0.6108 - val_loss: 0.7424 - val_mae: 0.6436
    Epoch 777/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5705 - mae: 0.5898 - val_loss: 0.9924 - val_mae: 0.7260
    Epoch 778/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.7748 - mae: 0.6868 - val_loss: 0.7964 - val_mae: 0.6839
    Epoch 779/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6030 - mae: 0.5921 - val_loss: 0.6706 - val_mae: 0.6284
    Epoch 780/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5070 - mae: 0.5850 - val_loss: 0.5121 - val_mae: 0.5380
    Epoch 781/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6485 - mae: 0.6080 - val_loss: 0.7282 - val_mae: 0.6054
    Epoch 782/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8581 - mae: 0.6758 - val_loss: 0.7206 - val_mae: 0.6161
    Epoch 783/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7434 - mae: 0.6489 - val_loss: 0.8513 - val_mae: 0.6758
    Epoch 784/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.5842 - mae: 0.5913 - val_loss: 0.7097 - val_mae: 0.6611
    Epoch 785/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5942 - mae: 0.5671 - val_loss: 0.5585 - val_mae: 0.5628
    Epoch 786/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6862 - mae: 0.6713 - val_loss: 0.7657 - val_mae: 0.6439
    Epoch 787/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.4479 - mae: 0.5286 - val_loss: 0.8709 - val_mae: 0.6772
    Epoch 788/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7984 - mae: 0.6683 - val_loss: 0.6583 - val_mae: 0.6276
    Epoch 789/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7781 - mae: 0.7030 - val_loss: 0.6089 - val_mae: 0.6057
    Epoch 790/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6767 - mae: 0.6634 - val_loss: 0.6661 - val_mae: 0.6637
    Epoch 791/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7512 - mae: 0.6181 - val_loss: 0.8290 - val_mae: 0.7269
    Epoch 792/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6502 - mae: 0.6016 - val_loss: 0.7032 - val_mae: 0.6068
    Epoch 793/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7822 - mae: 0.6715 - val_loss: 0.8699 - val_mae: 0.6929
    Epoch 794/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7589 - mae: 0.6910 - val_loss: 0.8895 - val_mae: 0.7424
    Epoch 795/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5748 - mae: 0.5598 - val_loss: 0.7877 - val_mae: 0.6564
    Epoch 796/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5964 - mae: 0.6095 - val_loss: 0.9342 - val_mae: 0.7294
    Epoch 797/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4429 - mae: 0.5266 - val_loss: 0.8548 - val_mae: 0.6907
    Epoch 798/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.8203 - mae: 0.6990 - val_loss: 0.7809 - val_mae: 0.6654
    Epoch 799/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7778 - mae: 0.6272 - val_loss: 0.9010 - val_mae: 0.6639
    Epoch 800/1000
    26/26 [==============================] - 4s 127ms/step - loss: 1.1513 - mae: 0.8218 - val_loss: 0.5908 - val_mae: 0.6100
    Epoch 801/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8229 - mae: 0.7066 - val_loss: 0.5874 - val_mae: 0.5981
    Epoch 802/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7067 - mae: 0.6488 - val_loss: 0.3862 - val_mae: 0.4528
    Epoch 803/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6071 - mae: 0.5896 - val_loss: 0.6450 - val_mae: 0.5649
    Epoch 804/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.5046 - mae: 0.5453 - val_loss: 0.4658 - val_mae: 0.5471
    Epoch 805/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6047 - mae: 0.5810 - val_loss: 0.7052 - val_mae: 0.6495
    Epoch 806/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8475 - mae: 0.7276 - val_loss: 0.8722 - val_mae: 0.7118
    Epoch 807/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7439 - mae: 0.6503 - val_loss: 0.8039 - val_mae: 0.7018
    Epoch 808/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.9426 - mae: 0.7448 - val_loss: 0.5254 - val_mae: 0.5557
    Epoch 809/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.9070 - mae: 0.7578 - val_loss: 0.5881 - val_mae: 0.5815
    Epoch 810/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6735 - mae: 0.6103 - val_loss: 0.7975 - val_mae: 0.6870
    Epoch 811/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6137 - mae: 0.5607 - val_loss: 1.0648 - val_mae: 0.7734
    Epoch 812/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6180 - mae: 0.5824 - val_loss: 0.5563 - val_mae: 0.5763
    Epoch 813/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6753 - mae: 0.6468 - val_loss: 0.4482 - val_mae: 0.5146
    Epoch 814/1000
    26/26 [==============================] - 6s 176ms/step - loss: 0.5656 - mae: 0.5574 - val_loss: 0.5682 - val_mae: 0.6082
    Epoch 815/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.6367 - mae: 0.5880 - val_loss: 0.5275 - val_mae: 0.5554
    Epoch 816/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6461 - mae: 0.6395 - val_loss: 0.6279 - val_mae: 0.6092
    Epoch 817/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7269 - mae: 0.6344 - val_loss: 0.6415 - val_mae: 0.6185
    Epoch 818/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6447 - mae: 0.5885 - val_loss: 0.6141 - val_mae: 0.5814
    Epoch 819/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.6565 - mae: 0.6053 - val_loss: 0.8575 - val_mae: 0.7023
    Epoch 820/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7465 - mae: 0.6186 - val_loss: 0.6031 - val_mae: 0.5869
    Epoch 821/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.4971 - mae: 0.5726 - val_loss: 0.7791 - val_mae: 0.6600
    Epoch 822/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.9324 - mae: 0.7321 - val_loss: 0.8309 - val_mae: 0.6501
    Epoch 823/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8492 - mae: 0.7093 - val_loss: 0.7162 - val_mae: 0.6147
    Epoch 824/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.8992 - mae: 0.7151 - val_loss: 0.6734 - val_mae: 0.6071
    Epoch 825/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.7624 - mae: 0.6668 - val_loss: 0.4596 - val_mae: 0.5335
    Epoch 826/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7058 - mae: 0.6351 - val_loss: 0.6518 - val_mae: 0.6222
    Epoch 827/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7882 - mae: 0.6928 - val_loss: 0.6077 - val_mae: 0.6216
    Epoch 828/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.7045 - mae: 0.6093 - val_loss: 0.8166 - val_mae: 0.6656
    Epoch 829/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.5826 - mae: 0.6045 - val_loss: 0.8049 - val_mae: 0.6869
    Epoch 830/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.8465 - mae: 0.6665 - val_loss: 0.6893 - val_mae: 0.6428
    Epoch 831/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.6707 - mae: 0.6622 - val_loss: 0.6247 - val_mae: 0.6228
    Epoch 832/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6785 - mae: 0.6089 - val_loss: 0.9577 - val_mae: 0.7325
    Epoch 833/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.9692 - mae: 0.7423 - val_loss: 0.6428 - val_mae: 0.6050
    Epoch 834/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.7507 - mae: 0.6510 - val_loss: 0.7405 - val_mae: 0.6724
    Epoch 835/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.7526 - mae: 0.6819 - val_loss: 0.7117 - val_mae: 0.6527
    Epoch 836/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7245 - mae: 0.6480 - val_loss: 0.8357 - val_mae: 0.7152
    Epoch 837/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6470 - mae: 0.6211 - val_loss: 0.6348 - val_mae: 0.6094
    Epoch 838/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7608 - mae: 0.6801 - val_loss: 0.7002 - val_mae: 0.6863
    Epoch 839/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.5608 - mae: 0.5776 - val_loss: 0.8651 - val_mae: 0.7224
    Epoch 840/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.6849 - mae: 0.6454 - val_loss: 0.4112 - val_mae: 0.5060
    Epoch 841/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7549 - mae: 0.6475 - val_loss: 0.9502 - val_mae: 0.7450
    Epoch 842/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.6546 - mae: 0.6088 - val_loss: 0.7890 - val_mae: 0.6733
    Epoch 843/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7801 - mae: 0.6314 - val_loss: 0.8942 - val_mae: 0.7330
    Epoch 844/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.8147 - mae: 0.7324 - val_loss: 0.6416 - val_mae: 0.5992
    Epoch 845/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.6179 - mae: 0.6235 - val_loss: 0.4988 - val_mae: 0.5424
    Epoch 846/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.8240 - mae: 0.7041 - val_loss: 0.7246 - val_mae: 0.6398
    Epoch 847/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.6833 - mae: 0.6190 - val_loss: 0.9185 - val_mae: 0.7541
    Epoch 848/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6751 - mae: 0.6496 - val_loss: 0.7025 - val_mae: 0.6297
    Epoch 849/1000
    26/26 [==============================] - 3s 95ms/step - loss: 0.6366 - mae: 0.6016 - val_loss: 0.8762 - val_mae: 0.7770
    Epoch 850/1000
    26/26 [==============================] - 3s 103ms/step - loss: 0.6948 - mae: 0.6220 - val_loss: 0.3874 - val_mae: 0.5045
    Epoch 851/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.4379 - mae: 0.5335 - val_loss: 0.6298 - val_mae: 0.6298
    Epoch 852/1000
    26/26 [==============================] - 4s 124ms/step - loss: 1.0748 - mae: 0.8410 - val_loss: 0.5708 - val_mae: 0.5940
    Epoch 853/1000
    26/26 [==============================] - 4s 101ms/step - loss: 0.6537 - mae: 0.6172 - val_loss: 0.6903 - val_mae: 0.6030
    Epoch 854/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.8910 - mae: 0.7321 - val_loss: 0.6262 - val_mae: 0.5525
    Epoch 855/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.7903 - mae: 0.6444 - val_loss: 0.6385 - val_mae: 0.6013
    Epoch 856/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7561 - mae: 0.6365 - val_loss: 0.7465 - val_mae: 0.6452
    Epoch 857/1000
    26/26 [==============================] - 4s 129ms/step - loss: 0.5136 - mae: 0.5243 - val_loss: 0.5604 - val_mae: 0.5775
    Epoch 858/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6599 - mae: 0.5964 - val_loss: 0.7044 - val_mae: 0.6430
    Epoch 859/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.4576 - mae: 0.5323 - val_loss: 0.7139 - val_mae: 0.6582
    Epoch 860/1000
    26/26 [==============================] - 4s 113ms/step - loss: 0.6824 - mae: 0.6234 - val_loss: 0.9199 - val_mae: 0.6909
    Epoch 861/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7013 - mae: 0.6716 - val_loss: 0.9422 - val_mae: 0.7241
    Epoch 862/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.6035 - mae: 0.5994 - val_loss: 0.5034 - val_mae: 0.5417
    Epoch 863/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6604 - mae: 0.5973 - val_loss: 0.6916 - val_mae: 0.6664
    Epoch 864/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.7425 - mae: 0.6399 - val_loss: 0.8679 - val_mae: 0.7020
    Epoch 865/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.4196 - mae: 0.5141 - val_loss: 0.6342 - val_mae: 0.6434
    Epoch 866/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5608 - mae: 0.5872 - val_loss: 0.9158 - val_mae: 0.7062
    Epoch 867/1000
    26/26 [==============================] - 4s 121ms/step - loss: 0.8912 - mae: 0.7019 - val_loss: 0.7268 - val_mae: 0.6695
    Epoch 868/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.4529 - mae: 0.4993 - val_loss: 0.7785 - val_mae: 0.6780
    Epoch 869/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.6277 - mae: 0.6142 - val_loss: 0.8416 - val_mae: 0.6968
    Epoch 870/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7609 - mae: 0.6496 - val_loss: 0.6462 - val_mae: 0.6482
    Epoch 871/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6554 - mae: 0.5985 - val_loss: 0.8955 - val_mae: 0.7196
    Epoch 872/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.5259 - mae: 0.5553 - val_loss: 0.9215 - val_mae: 0.7054
    Epoch 873/1000
    26/26 [==============================] - 4s 105ms/step - loss: 1.0080 - mae: 0.7820 - val_loss: 0.5074 - val_mae: 0.5627
    Epoch 874/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8153 - mae: 0.7099 - val_loss: 0.3972 - val_mae: 0.5194
    Epoch 875/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7047 - mae: 0.6656 - val_loss: 0.8897 - val_mae: 0.6994
    Epoch 876/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8198 - mae: 0.6643 - val_loss: 0.5463 - val_mae: 0.5641
    Epoch 877/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.6387 - mae: 0.6146 - val_loss: 0.5747 - val_mae: 0.5495
    Epoch 878/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6590 - mae: 0.5842 - val_loss: 0.6125 - val_mae: 0.5808
    Epoch 879/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.6627 - mae: 0.6323 - val_loss: 0.8255 - val_mae: 0.7435
    Epoch 880/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5420 - mae: 0.5649 - val_loss: 0.6201 - val_mae: 0.5923
    Epoch 881/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.5409 - mae: 0.6057 - val_loss: 0.6996 - val_mae: 0.6448
    Epoch 882/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6233 - mae: 0.5782 - val_loss: 0.8673 - val_mae: 0.7369
    Epoch 883/1000
    26/26 [==============================] - 4s 104ms/step - loss: 1.0117 - mae: 0.7779 - val_loss: 0.7630 - val_mae: 0.6487
    Epoch 884/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.9362 - mae: 0.7349 - val_loss: 0.5827 - val_mae: 0.5630
    Epoch 885/1000
    26/26 [==============================] - 4s 109ms/step - loss: 0.8234 - mae: 0.6436 - val_loss: 0.5148 - val_mae: 0.5700
    Epoch 886/1000
    26/26 [==============================] - 4s 111ms/step - loss: 0.6128 - mae: 0.6336 - val_loss: 0.6316 - val_mae: 0.6178
    Epoch 887/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.6988 - mae: 0.6452 - val_loss: 0.4775 - val_mae: 0.5388
    Epoch 888/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.9445 - mae: 0.7429 - val_loss: 0.9171 - val_mae: 0.6918
    Epoch 889/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.5509 - mae: 0.5911 - val_loss: 0.6241 - val_mae: 0.5625
    Epoch 890/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6953 - mae: 0.5938 - val_loss: 0.8694 - val_mae: 0.7227
    Epoch 891/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5225 - mae: 0.5564 - val_loss: 0.7257 - val_mae: 0.6268
    Epoch 892/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8997 - mae: 0.7109 - val_loss: 0.6783 - val_mae: 0.6156
    Epoch 893/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5432 - mae: 0.5348 - val_loss: 0.6701 - val_mae: 0.6341
    Epoch 894/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.5700 - mae: 0.5682 - val_loss: 0.6691 - val_mae: 0.6168
    Epoch 895/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.9167 - mae: 0.7379 - val_loss: 0.6493 - val_mae: 0.6165
    Epoch 896/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.8066 - mae: 0.7038 - val_loss: 0.8807 - val_mae: 0.6720
    Epoch 897/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8492 - mae: 0.6777 - val_loss: 0.6478 - val_mae: 0.6199
    Epoch 898/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4845 - mae: 0.5378 - val_loss: 1.0443 - val_mae: 0.7919
    Epoch 899/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.9113 - mae: 0.7146 - val_loss: 0.8134 - val_mae: 0.6571
    Epoch 900/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8315 - mae: 0.6762 - val_loss: 0.8161 - val_mae: 0.6961
    Epoch 901/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.8215 - mae: 0.6773 - val_loss: 0.7440 - val_mae: 0.6566
    Epoch 902/1000
    26/26 [==============================] - 4s 110ms/step - loss: 0.4816 - mae: 0.5640 - val_loss: 0.7853 - val_mae: 0.6628
    Epoch 903/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.7580 - mae: 0.6510 - val_loss: 0.7197 - val_mae: 0.6203
    Epoch 904/1000
    26/26 [==============================] - 3s 97ms/step - loss: 0.4059 - mae: 0.4898 - val_loss: 0.7716 - val_mae: 0.6527
    Epoch 905/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.6488 - mae: 0.6265 - val_loss: 0.6831 - val_mae: 0.6395
    Epoch 906/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6875 - mae: 0.6371 - val_loss: 0.5980 - val_mae: 0.5695
    Epoch 907/1000
    26/26 [==============================] - 4s 115ms/step - loss: 0.7273 - mae: 0.6586 - val_loss: 0.5657 - val_mae: 0.5337
    Epoch 908/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.8404 - mae: 0.7098 - val_loss: 0.6668 - val_mae: 0.6307
    Epoch 909/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8804 - mae: 0.7353 - val_loss: 0.6524 - val_mae: 0.6083
    Epoch 910/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.9935 - mae: 0.7701 - val_loss: 0.5807 - val_mae: 0.5719
    Epoch 911/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.6623 - mae: 0.6380 - val_loss: 0.7972 - val_mae: 0.6839
    Epoch 912/1000
    26/26 [==============================] - 4s 116ms/step - loss: 0.9722 - mae: 0.7063 - val_loss: 0.8094 - val_mae: 0.6802
    Epoch 913/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.6724 - mae: 0.6303 - val_loss: 0.6715 - val_mae: 0.6506
    Epoch 914/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.6689 - mae: 0.6273 - val_loss: 0.7805 - val_mae: 0.6467
    Epoch 915/1000
    26/26 [==============================] - 4s 118ms/step - loss: 0.6570 - mae: 0.6023 - val_loss: 0.9072 - val_mae: 0.7011
    Epoch 916/1000
    26/26 [==============================] - 4s 110ms/step - loss: 0.7716 - mae: 0.6789 - val_loss: 0.4632 - val_mae: 0.5306
    Epoch 917/1000
    26/26 [==============================] - 5s 147ms/step - loss: 0.5969 - mae: 0.5542 - val_loss: 0.7724 - val_mae: 0.6340
    Epoch 918/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.9135 - mae: 0.7232 - val_loss: 0.6933 - val_mae: 0.6574
    Epoch 919/1000
    26/26 [==============================] - 4s 114ms/step - loss: 0.8538 - mae: 0.7246 - val_loss: 0.5201 - val_mae: 0.5388
    Epoch 920/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7180 - mae: 0.6568 - val_loss: 0.6270 - val_mae: 0.6064
    Epoch 921/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.4787 - mae: 0.5099 - val_loss: 0.5105 - val_mae: 0.5298
    Epoch 922/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.8380 - mae: 0.7056 - val_loss: 0.8745 - val_mae: 0.6949
    Epoch 923/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6228 - mae: 0.5739 - val_loss: 0.6535 - val_mae: 0.6011
    Epoch 924/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.6736 - mae: 0.6322 - val_loss: 0.5780 - val_mae: 0.5633
    Epoch 925/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8159 - mae: 0.6679 - val_loss: 0.7950 - val_mae: 0.7018
    Epoch 926/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.4861 - mae: 0.4962 - val_loss: 0.5575 - val_mae: 0.5787
    Epoch 927/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7191 - mae: 0.6465 - val_loss: 0.5145 - val_mae: 0.5473
    Epoch 928/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.6796 - mae: 0.6035 - val_loss: 0.9046 - val_mae: 0.7816
    Epoch 929/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.7703 - mae: 0.6878 - val_loss: 0.5470 - val_mae: 0.6055
    Epoch 930/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.5905 - mae: 0.5794 - val_loss: 0.6801 - val_mae: 0.6441
    Epoch 931/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8253 - mae: 0.6836 - val_loss: 0.7856 - val_mae: 0.6511
    Epoch 932/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8670 - mae: 0.6717 - val_loss: 0.5996 - val_mae: 0.6163
    Epoch 933/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6529 - mae: 0.6112 - val_loss: 1.0465 - val_mae: 0.8045
    Epoch 934/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7325 - mae: 0.6398 - val_loss: 0.5281 - val_mae: 0.5633
    Epoch 935/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.9679 - mae: 0.7998 - val_loss: 0.7381 - val_mae: 0.6904
    Epoch 936/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7378 - mae: 0.6681 - val_loss: 0.6728 - val_mae: 0.6494
    Epoch 937/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4470 - mae: 0.4854 - val_loss: 0.4948 - val_mae: 0.5561
    Epoch 938/1000
    26/26 [==============================] - 6s 179ms/step - loss: 0.8084 - mae: 0.6784 - val_loss: 0.4787 - val_mae: 0.5419
    Epoch 939/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4885 - mae: 0.5187 - val_loss: 0.8589 - val_mae: 0.7484
    Epoch 940/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.9511 - mae: 0.7345 - val_loss: 0.7247 - val_mae: 0.6416
    Epoch 941/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7199 - mae: 0.6633 - val_loss: 0.5536 - val_mae: 0.5764
    Epoch 942/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6767 - mae: 0.5927 - val_loss: 0.8155 - val_mae: 0.7299
    Epoch 943/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.5316 - mae: 0.5542 - val_loss: 0.6069 - val_mae: 0.6255
    Epoch 944/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7354 - mae: 0.6544 - val_loss: 0.6312 - val_mae: 0.6294
    Epoch 945/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7316 - mae: 0.6507 - val_loss: 0.5891 - val_mae: 0.5638
    Epoch 946/1000
    26/26 [==============================] - 4s 106ms/step - loss: 0.4757 - mae: 0.5533 - val_loss: 0.6530 - val_mae: 0.6394
    Epoch 947/1000
    26/26 [==============================] - 4s 109ms/step - loss: 0.5946 - mae: 0.6085 - val_loss: 0.4743 - val_mae: 0.5027
    Epoch 948/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.4477 - mae: 0.5279 - val_loss: 0.7682 - val_mae: 0.6663
    Epoch 949/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.6862 - mae: 0.6766 - val_loss: 0.5231 - val_mae: 0.5553
    Epoch 950/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.7764 - mae: 0.6950 - val_loss: 0.7479 - val_mae: 0.6534
    Epoch 951/1000
    26/26 [==============================] - 4s 108ms/step - loss: 0.8510 - mae: 0.7192 - val_loss: 0.7722 - val_mae: 0.6432
    Epoch 952/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.7848 - mae: 0.6963 - val_loss: 0.6866 - val_mae: 0.6613
    Epoch 953/1000
    26/26 [==============================] - 3s 100ms/step - loss: 0.6412 - mae: 0.6141 - val_loss: 0.7190 - val_mae: 0.6453
    Epoch 954/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7349 - mae: 0.6646 - val_loss: 0.7851 - val_mae: 0.6679
    Epoch 955/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7825 - mae: 0.6597 - val_loss: 0.6825 - val_mae: 0.6224
    Epoch 956/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7519 - mae: 0.6528 - val_loss: 0.7204 - val_mae: 0.6502
    Epoch 957/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.7322 - mae: 0.6692 - val_loss: 0.5492 - val_mae: 0.5596
    Epoch 958/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.5930 - mae: 0.6315 - val_loss: 0.7717 - val_mae: 0.6600
    Epoch 959/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8239 - mae: 0.6531 - val_loss: 0.6083 - val_mae: 0.5804
    Epoch 960/1000
    26/26 [==============================] - 5s 159ms/step - loss: 0.8877 - mae: 0.7216 - val_loss: 0.6447 - val_mae: 0.5692
    Epoch 961/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7142 - mae: 0.6532 - val_loss: 0.4523 - val_mae: 0.5349
    Epoch 962/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6585 - mae: 0.6057 - val_loss: 0.6754 - val_mae: 0.6182
    Epoch 963/1000
    26/26 [==============================] - 4s 117ms/step - loss: 0.6698 - mae: 0.6179 - val_loss: 0.7479 - val_mae: 0.6458
    Epoch 964/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6243 - mae: 0.6261 - val_loss: 0.5676 - val_mae: 0.6134
    Epoch 965/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.8726 - mae: 0.6798 - val_loss: 0.6641 - val_mae: 0.6246
    Epoch 966/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7445 - mae: 0.6635 - val_loss: 0.6079 - val_mae: 0.6262
    Epoch 967/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.6932 - mae: 0.6689 - val_loss: 0.6136 - val_mae: 0.5953
    Epoch 968/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.6674 - mae: 0.6205 - val_loss: 0.9124 - val_mae: 0.7239
    Epoch 969/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.7381 - mae: 0.6574 - val_loss: 0.9694 - val_mae: 0.7754
    Epoch 970/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.5883 - mae: 0.5790 - val_loss: 0.6394 - val_mae: 0.6051
    Epoch 971/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5956 - mae: 0.6073 - val_loss: 0.7490 - val_mae: 0.6388
    Epoch 972/1000
    26/26 [==============================] - 4s 107ms/step - loss: 0.6572 - mae: 0.6189 - val_loss: 0.7312 - val_mae: 0.6359
    Epoch 973/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9498 - mae: 0.7142 - val_loss: 0.9418 - val_mae: 0.7255
    Epoch 974/1000
    26/26 [==============================] - 3s 102ms/step - loss: 0.8064 - mae: 0.6628 - val_loss: 0.6667 - val_mae: 0.6111
    Epoch 975/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.7672 - mae: 0.7029 - val_loss: 0.5948 - val_mae: 0.5837
    Epoch 976/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.8610 - mae: 0.7361 - val_loss: 0.6787 - val_mae: 0.6513
    Epoch 977/1000
    26/26 [==============================] - 4s 122ms/step - loss: 0.7283 - mae: 0.6507 - val_loss: 0.5799 - val_mae: 0.5520
    Epoch 978/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6900 - mae: 0.6182 - val_loss: 0.7244 - val_mae: 0.6427
    Epoch 979/1000
    26/26 [==============================] - 4s 102ms/step - loss: 0.6931 - mae: 0.6367 - val_loss: 0.7549 - val_mae: 0.6872
    Epoch 980/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.8338 - mae: 0.6971 - val_loss: 0.9689 - val_mae: 0.7756
    Epoch 981/1000
    26/26 [==============================] - 4s 103ms/step - loss: 0.9501 - mae: 0.7737 - val_loss: 1.0249 - val_mae: 0.7848
    Epoch 982/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.6681 - mae: 0.6181 - val_loss: 0.7024 - val_mae: 0.6167
    Epoch 983/1000
    26/26 [==============================] - 3s 101ms/step - loss: 1.0539 - mae: 0.8085 - val_loss: 0.8588 - val_mae: 0.7145
    Epoch 984/1000
    26/26 [==============================] - 3s 101ms/step - loss: 0.7320 - mae: 0.6480 - val_loss: 0.6554 - val_mae: 0.6345
    Epoch 985/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.5262 - mae: 0.5857 - val_loss: 0.6411 - val_mae: 0.5991
    Epoch 986/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7231 - mae: 0.6314 - val_loss: 0.8546 - val_mae: 0.6810
    Epoch 987/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4545 - mae: 0.5369 - val_loss: 0.7771 - val_mae: 0.6438
    Epoch 988/1000
    26/26 [==============================] - 4s 105ms/step - loss: 0.8113 - mae: 0.6890 - val_loss: 0.7154 - val_mae: 0.6347
    Epoch 989/1000
    26/26 [==============================] - 4s 104ms/step - loss: 0.8688 - mae: 0.6713 - val_loss: 0.7992 - val_mae: 0.7024
    Epoch 990/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4535 - mae: 0.5101 - val_loss: 0.7231 - val_mae: 0.6494
    Epoch 991/1000
    26/26 [==============================] - 6s 181ms/step - loss: 0.6378 - mae: 0.5613 - val_loss: 0.8315 - val_mae: 0.6966
    Epoch 992/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7000 - mae: 0.6195 - val_loss: 1.1400 - val_mae: 0.8480
    Epoch 993/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.4337 - mae: 0.5400 - val_loss: 0.8080 - val_mae: 0.7121
    Epoch 994/1000
    26/26 [==============================] - 4s 125ms/step - loss: 0.8286 - mae: 0.6669 - val_loss: 0.8136 - val_mae: 0.6761
    Epoch 995/1000
    26/26 [==============================] - 4s 123ms/step - loss: 0.5415 - mae: 0.5747 - val_loss: 0.4610 - val_mae: 0.5219
    Epoch 996/1000
    26/26 [==============================] - 4s 124ms/step - loss: 0.7596 - mae: 0.6575 - val_loss: 0.3905 - val_mae: 0.4579
    Epoch 997/1000
    26/26 [==============================] - 5s 147ms/step - loss: 0.6040 - mae: 0.6031 - val_loss: 0.7955 - val_mae: 0.7023
    Epoch 998/1000
    26/26 [==============================] - 4s 127ms/step - loss: 0.7426 - mae: 0.6449 - val_loss: 0.8155 - val_mae: 0.7049
    Epoch 999/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7266 - mae: 0.6583 - val_loss: 0.6853 - val_mae: 0.6335
    Epoch 1000/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8341 - mae: 0.6691 - val_loss: 0.7582 - val_mae: 0.6499


Now, we load the infos and evaluate the net:


```python
##################Checking History############################
model = keras.models.load_model('one_file.keras')
print(f"Test MAE: {model.evaluate(test,batch_size=batch_size,steps=int(test_count/batch_size))[1]:.2f}")
loss = history.history["mae"]
val_loss = history.history["val_mae"]
epochs = range(1, len(loss) + 1)
```

    26/26 [==============================] - 0s 2ms/step - loss: 0.5691 - mae: 0.5600
    Test MAE: 0.56



```python
first_layer_weights = model.layers[0].get_weights()[0]
first_layer_biases  = model.layers[0].get_weights()[1]
print("w = "+str(first_layer_weights[0][0]))
print("b = "+str(first_layer_biases[0]))
print()
print("History of weight: \n"+str(gw.weight_dict['w_']))
print("History of bias: \n"+str(gw.weight_dict['b_']))
```

    w = 0.70854586
    b = 0.4554484
    
    History of weight: 
    [[[-1.48527575e+00 -1.46283615e+00 -1.43991995e+00 -1.41954553e+00
       -1.39996886e+00 -1.37924457e+00 -1.35924637e+00 -1.34033155e+00
       -1.31901765e+00 -1.29836893e+00 -1.27747130e+00 -1.25733423e+00
       -1.23601079e+00 -1.21356070e+00 -1.19210744e+00 -1.17397618e+00
       -1.15627682e+00 -1.13501525e+00 -1.11299706e+00 -1.09612942e+00
       -1.07779348e+00 -1.05634749e+00 -1.03427052e+00 -1.01373005e+00
       -9.91942406e-01 -9.72253919e-01 -9.53821242e-01 -9.35765564e-01
       -9.17218983e-01 -8.99446547e-01 -8.82888854e-01 -8.66963029e-01
       -8.48021030e-01 -8.31621945e-01 -8.13902378e-01 -7.97376096e-01
       -7.78454006e-01 -7.62628555e-01 -7.45060027e-01 -7.26956606e-01
       -7.11052656e-01 -6.95244133e-01 -6.79183304e-01 -6.63669646e-01
       -6.48601234e-01 -6.32753432e-01 -6.15988791e-01 -5.99143922e-01
       -5.84217250e-01 -5.67509115e-01 -5.52542150e-01 -5.36573470e-01
       -5.21257937e-01 -5.04943073e-01 -4.90940183e-01 -4.77126658e-01
       -4.64051247e-01 -4.50178176e-01 -4.35466141e-01 -4.21777338e-01
       -4.06580001e-01 -3.90029669e-01 -3.74611557e-01 -3.62006098e-01
       -3.50322604e-01 -3.39142740e-01 -3.27414572e-01 -3.13400179e-01
       -2.99665183e-01 -2.88877219e-01 -2.77606636e-01 -2.66336858e-01
       -2.54827112e-01 -2.43018329e-01 -2.34520286e-01 -2.23427057e-01
       -2.11863413e-01 -2.02509344e-01 -1.90392062e-01 -1.81345254e-01
       -1.74386173e-01 -1.65420324e-01 -1.57441676e-01 -1.46554276e-01
       -1.35423765e-01 -1.25555649e-01 -1.15481898e-01 -1.06691428e-01
       -9.88873988e-02 -9.09943581e-02 -8.18481594e-02 -7.30853751e-02
       -6.47859201e-02 -5.66994697e-02 -4.78140339e-02 -4.03259099e-02
       -3.22829261e-02 -2.47558970e-02 -1.71709768e-02 -7.84458872e-03
        2.77478626e-04  8.40756670e-03  1.50161115e-02  2.27694493e-02
        2.91205365e-02  3.53453942e-02  4.11851183e-02  4.81464341e-02
        5.80071099e-02  6.78267628e-02  7.53654540e-02  8.13826323e-02
        8.76473859e-02  9.49041098e-02  1.00279294e-01  1.04840420e-01
        1.09739088e-01  1.14856653e-01  1.22702077e-01  1.27636328e-01
        1.32477030e-01  1.37874469e-01  1.43776402e-01  1.49512798e-01
        1.53729513e-01  1.59000292e-01  1.63913280e-01  1.69414610e-01
        1.74265295e-01  1.76415145e-01  1.81713492e-01  1.89225301e-01
        1.94701076e-01  1.97782636e-01  2.00449154e-01  2.02958375e-01
        2.09589571e-01  2.18227729e-01  2.23978356e-01  2.28899583e-01
        2.36153483e-01  2.42259994e-01  2.47810572e-01  2.51418710e-01
        2.54860461e-01  2.58596003e-01  2.63409793e-01  2.66184419e-01
        2.71468282e-01  2.72984415e-01  2.75865465e-01  2.78581142e-01
        2.82747537e-01  2.84054339e-01  2.85579741e-01  2.89188415e-01
        2.95623094e-01  3.04015905e-01  3.10575902e-01  3.13996971e-01
        3.18459809e-01  3.22176129e-01  3.30389977e-01  3.33973318e-01
        3.38343978e-01  3.38788271e-01  3.40745270e-01  3.40066165e-01
        3.44824821e-01  3.45144957e-01  3.48759681e-01  3.46574634e-01
        3.47326308e-01  3.51828158e-01  3.56993288e-01  3.61772895e-01
        3.60964119e-01  3.60496253e-01  3.70138705e-01  3.76933038e-01
        3.80187541e-01  3.80863607e-01  3.90060395e-01  3.98822427e-01
        4.05251682e-01  4.09478724e-01  4.07187790e-01  4.07521874e-01
        4.02944505e-01  4.03376162e-01  4.07403231e-01  4.11943197e-01
        4.15428966e-01  4.19418484e-01  4.21570390e-01  4.20583338e-01
        4.21245307e-01  4.26291138e-01  4.30592090e-01  4.36838210e-01
        4.35113549e-01  4.36483443e-01  4.33625221e-01  4.34649915e-01
        4.35601264e-01  4.38360691e-01  4.42182004e-01  4.43154901e-01
        4.48349863e-01  4.60068822e-01  4.60265547e-01  4.68179494e-01
        4.74031091e-01  4.74967599e-01  4.71406937e-01  4.71267492e-01
        4.73787814e-01  4.75122333e-01  4.74502265e-01  4.74318832e-01
        4.76071745e-01  4.77131307e-01  4.84815627e-01  4.90956098e-01
        4.96146500e-01  4.98724163e-01  4.98109847e-01  4.99203742e-01
        4.97313827e-01  4.94864374e-01  5.03935158e-01  5.08428097e-01
        5.11164546e-01  5.08245587e-01  5.12088060e-01  5.17183900e-01
        5.24012387e-01  5.26435673e-01  5.28503239e-01  5.34119189e-01
        5.38466334e-01  5.40027499e-01  5.44930220e-01  5.48094630e-01
        5.50837219e-01  5.49187541e-01  5.46001375e-01  5.51892638e-01
        5.54790974e-01  5.57976425e-01  5.60794473e-01  5.62281668e-01
        5.63270867e-01  5.62169611e-01  5.70420742e-01  5.75321257e-01
        5.74508667e-01  5.72693706e-01  5.75733066e-01  5.77189386e-01
        5.76658249e-01  5.77764869e-01  5.81598699e-01  5.86722434e-01
        5.83655775e-01  5.82800806e-01  5.84100604e-01  5.88092029e-01
        5.84462941e-01  5.81428349e-01  5.76445878e-01  5.78425407e-01
        5.76846480e-01  5.77358305e-01  5.76794326e-01  5.84232748e-01
        5.85738003e-01  5.88324547e-01  5.89688957e-01  5.93135238e-01
        5.97280562e-01  6.00669920e-01  6.04326129e-01  6.05301142e-01
        6.08417749e-01  6.17244363e-01  6.09312952e-01  6.14179611e-01
        6.12671316e-01  6.11368537e-01  6.09973192e-01  6.08013868e-01
        6.14737272e-01  6.17855132e-01  6.14975631e-01  6.13463700e-01
        6.10386729e-01  6.08489037e-01  6.05059624e-01  6.08605862e-01
        6.10569954e-01  6.11108243e-01  6.16756260e-01  6.08393669e-01
        6.08738661e-01  6.03893638e-01  5.97366333e-01  5.99981904e-01
        6.02243066e-01  6.07409358e-01  6.05046213e-01  6.10189259e-01
        6.16362035e-01  6.11253679e-01  6.05161428e-01  6.08159900e-01
        6.08486474e-01  6.09610081e-01  6.10259473e-01  6.17261231e-01
        6.21536255e-01  6.21843338e-01  6.23135567e-01  6.26326561e-01
        6.31175101e-01  6.39165401e-01  6.48641050e-01  6.49680376e-01
        6.56197667e-01  6.58140302e-01  6.54371202e-01  6.49785578e-01
        6.51926219e-01  6.49888694e-01  6.46564722e-01  6.48216486e-01
        6.51961803e-01  6.53281569e-01  6.49600446e-01  6.55436099e-01
        6.62089586e-01  6.68326497e-01  6.68230295e-01  6.56646967e-01
        6.49984837e-01  6.50722742e-01  6.48681164e-01  6.54482841e-01
        6.56539142e-01  6.55464888e-01  6.52063847e-01  6.50929570e-01
        6.50936186e-01  6.53680027e-01  6.56321406e-01  6.60250127e-01
        6.63357496e-01  6.62255108e-01  6.60801828e-01  6.64975107e-01
        6.62579000e-01  6.62917554e-01  6.57826960e-01  6.53090239e-01
        6.52421832e-01  6.54569149e-01  6.52772784e-01  6.54063344e-01
        6.56623423e-01  6.61002338e-01  6.68052077e-01  6.64938629e-01
        6.58248305e-01  6.56292260e-01  6.52641356e-01  6.46840990e-01
        6.45599127e-01  6.47408545e-01  6.49914265e-01  6.53818369e-01
        6.56848907e-01  6.56721711e-01  6.51233137e-01  6.51821077e-01
        6.58288181e-01  6.60383821e-01  6.59256816e-01  6.59889996e-01
        6.53560102e-01  6.55694544e-01  6.60437047e-01  6.65492594e-01
        6.71072364e-01  6.69535398e-01  6.65771484e-01  6.72694623e-01
        6.71334863e-01  6.69878900e-01  6.73114419e-01  6.70202971e-01
        6.70635760e-01  6.72005653e-01  6.76586270e-01  6.77488029e-01
        6.70181274e-01  6.70549929e-01  6.65911078e-01  6.66686654e-01
        6.68637693e-01  6.68199241e-01  6.73774898e-01  6.72801137e-01
        6.66937828e-01  6.64469182e-01  6.65831506e-01  6.63472474e-01
        6.60119057e-01  6.65986836e-01  6.74590766e-01  6.81537211e-01
        6.83150887e-01  6.83850706e-01  6.76226318e-01  6.68417335e-01
        6.63298607e-01  6.61807895e-01  6.65916502e-01  6.67819798e-01
        6.70222819e-01  6.74339890e-01  6.74123406e-01  6.75828695e-01
        6.79875314e-01  6.81182921e-01  6.84254706e-01  6.79220676e-01
        6.78857327e-01  6.68594003e-01  6.67984486e-01  6.73098743e-01
        6.78634942e-01  6.81166291e-01  6.87067389e-01  6.83193982e-01
        6.78997815e-01  6.79674983e-01  6.79986537e-01  6.76790059e-01
        6.77312016e-01  6.75294518e-01  6.77076638e-01  6.75285459e-01
        6.81095183e-01  6.84186995e-01  6.90791011e-01  6.88811660e-01
        6.90285921e-01  6.85848713e-01  6.85985327e-01  6.83851898e-01
        6.84594989e-01  6.87776864e-01  6.86833680e-01  6.92805886e-01
        6.90403879e-01  6.86658680e-01  6.88984752e-01  6.83849633e-01
        6.81481659e-01  6.80418015e-01  6.82150066e-01  6.85309112e-01
        6.90469980e-01  6.97019756e-01  6.93870664e-01  6.93860233e-01
        6.96223378e-01  7.02335417e-01  7.08449125e-01  7.08371341e-01
        7.08107710e-01  7.10307419e-01  7.07415164e-01  7.06519544e-01
        7.09988117e-01  7.14712918e-01  7.19330311e-01  7.10487068e-01
        7.09496081e-01  7.10751534e-01  7.10284173e-01  7.10272789e-01
        7.10991144e-01  7.04745352e-01  7.03762174e-01  6.99726582e-01
        7.00465679e-01  7.02830374e-01  6.96093142e-01  6.88390791e-01
        6.86902165e-01  6.89751685e-01  6.95385754e-01  6.92664146e-01
        6.93847120e-01  6.97581649e-01  7.07339346e-01  7.13500500e-01
        7.13561773e-01  7.13567078e-01  7.09758759e-01  7.06639707e-01
        7.08680987e-01  7.06321120e-01  7.00082123e-01  6.99217916e-01
        7.01516628e-01  7.05235004e-01  6.98063314e-01  6.92332625e-01
        6.93170369e-01  6.94099545e-01  6.91829383e-01  6.93735778e-01
        6.92454398e-01  6.94097221e-01  6.86740756e-01  6.87812686e-01
        6.92597568e-01  6.91446662e-01  6.96550548e-01  7.00116813e-01
        7.00519264e-01  7.00537086e-01  6.98010325e-01  7.00444996e-01
        7.01648057e-01  7.04926491e-01  7.05580533e-01  7.03904748e-01
        7.05271482e-01  7.06304967e-01  7.05329716e-01  7.09011793e-01
        7.03442752e-01  6.99619949e-01  6.97805882e-01  6.95051193e-01
        7.01869607e-01  7.05518305e-01  7.05496669e-01  7.04737425e-01
        7.02188969e-01  7.01850533e-01  7.00399876e-01  6.98671877e-01
        7.06536710e-01  7.09000468e-01  7.12754905e-01  7.16364622e-01
        7.16118515e-01  7.17672825e-01  7.17145562e-01  7.12862670e-01
        7.10465670e-01  7.06060410e-01  7.05653191e-01  7.01870739e-01
        7.06264675e-01  7.05964267e-01  7.05642521e-01  7.06710756e-01
        7.08564699e-01  7.11228073e-01  7.14218497e-01  7.09340394e-01
        7.03053176e-01  7.02532649e-01  6.99320436e-01  6.99326694e-01
        7.02596247e-01  7.05835819e-01  7.06521273e-01  7.03725576e-01
        6.99897528e-01  7.01485395e-01  7.01482415e-01  7.00745165e-01
        7.02117205e-01  6.99424922e-01  6.96047783e-01  6.94541693e-01
        6.96985066e-01  6.97934747e-01  6.92339540e-01  6.97153926e-01
        7.00014055e-01  7.04977870e-01  7.08545864e-01  7.05583513e-01
        7.05381989e-01  6.98226511e-01  6.96067393e-01  6.96815014e-01
        6.94166124e-01  6.95326328e-01  6.99945986e-01  7.00038373e-01
        7.04110920e-01  6.94918156e-01  6.83822691e-01  6.87192202e-01
        6.94627464e-01  6.94131494e-01  6.79600537e-01  6.75351024e-01
        6.82355523e-01  6.86484158e-01  6.93769097e-01  6.93566859e-01
        6.94947720e-01  6.98274255e-01  7.01755941e-01  7.00599492e-01
        7.02601016e-01  7.05683231e-01  7.10828900e-01  7.13568509e-01
        7.12502182e-01  7.11545110e-01  7.13164449e-01  7.22607434e-01
        7.24285305e-01  7.21847713e-01  7.12121308e-01  7.06986248e-01
        7.05035985e-01  7.05586612e-01  7.00399697e-01  6.91709518e-01
        6.94771528e-01  6.93911552e-01  6.98883533e-01  7.00145543e-01
        7.06358075e-01  7.01868355e-01  7.00956464e-01  7.02362835e-01
        7.01301277e-01  7.02998161e-01  7.01907814e-01  6.99149668e-01
        7.02984929e-01  7.03659475e-01  7.07164824e-01  7.14119673e-01
        7.13414431e-01  7.11949825e-01  7.09502935e-01  7.06448436e-01
        7.06758082e-01  7.06886232e-01  7.12204218e-01  7.13755071e-01
        7.19020009e-01  7.08510756e-01  7.09746718e-01  7.07765520e-01
        7.05185175e-01  6.95520461e-01  6.97124124e-01  6.93051636e-01
        6.89514756e-01  6.90090299e-01  6.89132571e-01  6.86057508e-01
        6.88354015e-01  6.92295194e-01  6.84121311e-01  6.84836149e-01
        6.86228037e-01  6.81549311e-01  6.88109636e-01  6.89833462e-01
        6.91979170e-01  6.92058802e-01  7.00278223e-01  6.92912638e-01
        6.90749884e-01  6.90309703e-01  6.94614172e-01  6.99740529e-01
        7.02270091e-01  7.03193069e-01  7.04432905e-01  6.97013497e-01
        6.96858525e-01  7.00689554e-01  6.98655009e-01  7.02927768e-01
        7.07780838e-01  7.11050153e-01  7.08888829e-01  7.09586799e-01
        7.11973131e-01  7.09885061e-01  7.02126741e-01  7.03913569e-01
        7.09287822e-01  7.15336859e-01  7.13433027e-01  7.09109724e-01
        7.08938003e-01  6.95218921e-01  6.89940929e-01  6.91922426e-01
        6.91090405e-01  6.94923103e-01  6.97319388e-01  7.07585275e-01
        7.08807409e-01  7.13184416e-01  7.15278149e-01  7.17201114e-01
        7.14031518e-01  7.07074285e-01  7.03107595e-01  7.04693556e-01
        7.02031136e-01  7.06548750e-01  7.03175008e-01  6.98049486e-01
        6.98449373e-01  6.97583556e-01  7.01478183e-01  7.08525360e-01
        7.11271107e-01  7.11225510e-01  7.16201484e-01  7.13073254e-01
        7.10158408e-01  7.08611906e-01  7.13742852e-01  7.15968132e-01
        7.14535713e-01  7.14041948e-01  7.10213780e-01  7.03878403e-01
        7.06882715e-01  7.10674524e-01  7.09488094e-01  7.08260000e-01
        7.07009315e-01  7.10180163e-01  7.12587833e-01  7.13339031e-01
        7.16610134e-01  7.16744661e-01  7.18975961e-01  7.11805105e-01
        7.04482675e-01  7.05479085e-01  7.05389500e-01  6.99981987e-01
        7.01114714e-01  7.05532491e-01  7.07954228e-01  7.00474203e-01
        7.04298556e-01  7.06378520e-01  7.03057110e-01  7.03290522e-01
        7.06453264e-01  7.08534777e-01  7.09068835e-01  7.08776116e-01
        7.07512796e-01  7.03424037e-01  7.02561677e-01  7.03555882e-01
        7.00627863e-01  6.96326792e-01  7.02222049e-01  7.03378618e-01
        7.06900120e-01  7.02332318e-01  6.97673202e-01  6.95595980e-01
        6.93463624e-01  6.85945213e-01  6.92964494e-01  6.96735919e-01
        6.97455883e-01  7.01495528e-01  7.04258740e-01  7.03753293e-01
        7.04162717e-01  7.01798975e-01  7.09956527e-01  7.16546357e-01
        7.20715582e-01  7.23978698e-01  7.21103907e-01  7.23202586e-01
        7.24970341e-01  7.24294424e-01  7.23777175e-01  7.17545152e-01
        7.08373964e-01  7.02903450e-01  7.07270622e-01  7.12247074e-01
        7.08682001e-01  7.11834133e-01  7.16967225e-01  7.11497545e-01
        7.12387145e-01  7.15954900e-01  7.21379936e-01  7.26731777e-01
        7.21884787e-01  7.15884149e-01  7.11274803e-01  7.12678671e-01
        7.15067983e-01  7.15561450e-01  7.12400854e-01  7.16303229e-01
        7.13405728e-01  7.16915786e-01  7.12738097e-01  7.14466989e-01
        7.16864049e-01  7.25960851e-01  7.26401210e-01  7.25349367e-01
        7.23477781e-01  7.24124193e-01  7.23926127e-01  7.19988108e-01
        7.20453262e-01  7.22636759e-01  7.27670014e-01  7.23685682e-01
        7.18670428e-01  7.11429894e-01  7.14735925e-01  7.17496395e-01
        7.18497157e-01  7.22341180e-01  7.22045064e-01  7.10815191e-01
        7.08017886e-01  6.98803306e-01  6.97630346e-01  7.01463819e-01
        7.05078125e-01  7.12001741e-01  7.13584900e-01  7.08942950e-01
        7.05159366e-01  7.00067878e-01  7.05427468e-01  7.14285731e-01
        7.16748655e-01  7.19307125e-01  7.20527768e-01  7.24634469e-01
        7.20330715e-01  7.24458992e-01  7.26561427e-01  7.27183700e-01
        7.26126671e-01  7.21604228e-01  7.08194494e-01  7.02859700e-01
        6.99683666e-01  7.03566432e-01  7.03063011e-01  7.03765571e-01
        7.01259911e-01  7.08337665e-01  7.07652867e-01  7.05211818e-01
        7.06482828e-01  6.98167801e-01  6.95256770e-01  6.98538721e-01
        6.93252861e-01  6.95445836e-01  6.97017014e-01  6.99548721e-01
        7.06540823e-01  7.11065233e-01  7.09257305e-01  7.05385745e-01
        7.06204116e-01  7.06770837e-01  7.05233812e-01  7.09498286e-01
        7.09542215e-01  7.08520651e-01  7.06134140e-01  7.08100021e-01
        7.10290074e-01  7.10872829e-01  7.08391130e-01  7.03435242e-01
        7.01208055e-01  7.00824142e-01  7.00164258e-01  7.07549095e-01
        7.10531116e-01  7.11341023e-01  7.09048331e-01  7.12801874e-01
        7.18459308e-01  7.22211778e-01  7.16169953e-01  7.18993783e-01
        7.22124338e-01  7.22002625e-01  7.23052025e-01  7.21587837e-01
        7.22559392e-01  7.30242968e-01  7.28497028e-01  7.29757488e-01
        7.28688121e-01  7.28259325e-01  7.26329148e-01  7.31381118e-01
        7.30630934e-01  7.31293142e-01  7.26164699e-01  7.21822500e-01
        7.25167632e-01  7.29668796e-01  7.34990716e-01  7.32140899e-01
        7.31098771e-01  7.29717374e-01  7.33173072e-01  7.28957295e-01
        7.30746269e-01  7.33112872e-01  7.29317904e-01  7.29269564e-01
        7.19008446e-01  7.09117174e-01  7.01213241e-01  6.96077824e-01
        6.92728400e-01  6.91908836e-01  6.95917487e-01  6.97890401e-01
        6.95571005e-01  6.86575174e-01  6.91418707e-01  6.98273122e-01
        6.96649432e-01  7.03571200e-01  7.06889927e-01  7.08707631e-01
        7.13213623e-01  7.15550542e-01  7.09966421e-01  7.07885504e-01
        7.12333739e-01  7.14819074e-01  7.12197423e-01  7.07984090e-01
        7.15601325e-01  7.20829129e-01  7.12529242e-01  7.04643786e-01
        7.03898370e-01  7.07014203e-01  7.06206858e-01  7.08737791e-01
        7.05780566e-01  7.03879416e-01  7.02577531e-01  6.98938549e-01
        6.90744340e-01  6.90382004e-01  6.89993382e-01  6.90483212e-01
        6.94516122e-01  7.00218022e-01  7.07353413e-01  7.13163137e-01
        7.11501479e-01  7.13616192e-01  7.18799472e-01  7.20217168e-01
        7.15681195e-01  7.18091190e-01  7.14157224e-01  7.12311327e-01]]]
    History of bias: 
    [[[0.0258447  0.04986168 0.07449214 0.09769076 0.12025788 0.14377463
       0.16708355 0.18985195 0.2137599  0.23731452 0.26004398 0.28191623
       0.30426583 0.32790667 0.35146946 0.37262875 0.39337906 0.41602066
       0.43871772 0.45907307 0.48004383 0.5024381  0.52376986 0.5448895
       0.567129   0.5879912  0.607314   0.6268498  0.64601576 0.6644246
       0.6824218  0.6992059  0.71906304 0.73729956 0.75537264 0.7725119
       0.7918973  0.8091945  0.8268201  0.8444083  0.86016434 0.8752785
       0.8913256  0.9073028  0.9212021  0.93655694 0.9521132  0.968201
       0.98299515 0.9981393  1.0124587  1.027511   1.0408311  1.0554371
       1.068495   1.0811516  1.0926101  1.1041816  1.117087   1.1285992
       1.1415673  1.1557921  1.1688482  1.1806219  1.1909059  1.1999112
       1.2098454  1.2218279  1.2330304  1.2407568  1.2491971  1.2582706
       1.2680554  1.2772945  1.2820433  1.2891104  1.2961575  1.3017821
       1.310524   1.3156376  1.3195204  1.3246425  1.328681   1.3354111
       1.3430934  1.3491765  1.3567544  1.3626382  1.3664533  1.3700687
       1.3738632  1.3766205  1.3810309  1.3842821  1.3884301  1.3902311
       1.3933104  1.3958418  1.3973391  1.4022653  1.4039309  1.4060484
       1.4061794  1.4067786  1.4066031  1.4065603  1.4054645  1.4049822
       1.4083836  1.4119397  1.4130479  1.4117982  1.4111115  1.4122077
       1.4096134  1.4066086  1.4019061  1.3985469  1.4001974  1.3975166
       1.3932645  1.390692   1.3886818  1.3865839  1.3829035  1.3799497
       1.3762934  1.3730758  1.3685217  1.3617755  1.359017   1.3572118
       1.3539294  1.3476019  1.3400794  1.3326848  1.3317946  1.3326906
       1.3301294  1.327063   1.3250886  1.322844   1.3207991  1.3145208
       1.308209   1.300943   1.2950898  1.2880424  1.2840784  1.2738429
       1.2658813  1.2590533  1.2549195  1.2466954  1.2385617  1.2314433
       1.2297536  1.2286707  1.2251631  1.2181904  1.2140403  1.2088672
       1.2086633  1.2006724  1.1958762  1.1859236  1.1773801  1.1662384
       1.1620215  1.1516384  1.1467105  1.1351585  1.1246524  1.1203907
       1.1181418  1.1131384  1.1014918  1.0899318  1.0913519  1.0905474
       1.0865105  1.0778439  1.0807292  1.0843     1.0817198  1.0779039
       1.066595   1.0599548  1.0475281  1.0413235  1.0367126  1.0312977
       1.0260406  1.0204854  1.0139606  1.0049547  0.997428   0.9927931
       0.9874509  0.9844979  0.97391546 0.9675758  0.9562421  0.95027834
       0.94436467 0.9391216  0.9332004  0.9271053  0.92416435 0.9277472
       0.92040396 0.9208232  0.92016536 0.9161987  0.90475196 0.89880043
       0.89492464 0.889655   0.8835421  0.8806164  0.8754799  0.8683568
       0.86907375 0.86658925 0.8634649  0.8599407  0.8532016  0.85017306
       0.8443272  0.83492374 0.83777344 0.8332191  0.828798   0.8211041
       0.8183384  0.81669927 0.8184867  0.81673497 0.8125908  0.81257606
       0.80819815 0.8031843  0.8030178  0.80084234 0.7991256  0.7911096
       0.78222454 0.7844849  0.7819028  0.7806069  0.7790333  0.7720772
       0.76678985 0.7609472  0.76629555 0.7689129  0.7626936  0.7573108
       0.75638926 0.7553465  0.7485477  0.74550366 0.7438144  0.74318063
       0.7346881  0.72903234 0.7267446  0.7252199  0.7189807  0.71362966
       0.70366895 0.70026326 0.6933842  0.68749195 0.6844826  0.68751085
       0.6840921  0.68212736 0.6800842  0.67830974 0.6801521  0.67910475
       0.6767637  0.6758982  0.6737581  0.6778297  0.66611207 0.6664384
       0.6615312  0.6549702  0.65156525 0.6475495  0.6505406  0.65232563
       0.6458485  0.6416521  0.63324475 0.6298769  0.62512344 0.62538743
       0.62470025 0.6247318  0.6255596  0.6147777  0.611036   0.6023194
       0.5937788  0.59457505 0.5938303  0.5988207  0.59243107 0.5918482
       0.59169346 0.5854766  0.5781021  0.5803822  0.57937443 0.5754404
       0.5719666  0.5734409  0.57705295 0.573812   0.5735186  0.5739896
       0.5768555  0.58162296 0.58808655 0.58618236 0.58802634 0.5877967
       0.58262026 0.57609063 0.5783342  0.57689095 0.57237107 0.571152
       0.5706446  0.56714946 0.5610559  0.5649959  0.56821847 0.5740822
       0.5724259  0.5585176  0.5498309  0.5475621  0.54307115 0.5468749
       0.549823   0.54697907 0.5421082  0.5369151  0.53541046 0.5356574
       0.53742415 0.5371037  0.5379161  0.5336052  0.528771   0.5309552
       0.5256612  0.52417874 0.51642734 0.5104005  0.5066229  0.5076727
       0.50515366 0.50746506 0.5074926  0.5112698  0.5171397  0.5147461
       0.50640583 0.50505704 0.4992408  0.49252015 0.49134436 0.49404684
       0.49430674 0.49860027 0.5040539  0.50643414 0.5036394  0.50432223
       0.51185995 0.51202625 0.5079014  0.5083592  0.50155956 0.5013071
       0.5050832  0.5094111  0.51444674 0.5104799  0.50676537 0.5126851
       0.5118478  0.5099497  0.513476   0.5090193  0.5083031  0.5099208
       0.5129579  0.51030034 0.49988762 0.5002315  0.49499598 0.4927928
       0.49490342 0.49529353 0.5031283  0.5017729  0.4959443  0.4960208
       0.49572626 0.4926811  0.48537424 0.48905978 0.49545535 0.50063455
       0.5018781  0.5027105  0.494006   0.48669913 0.48125258 0.47849733
       0.47989702 0.48094442 0.48617086 0.4881509  0.48782682 0.49179876
       0.49634796 0.4989794  0.5021781  0.4961495  0.49486932 0.48349655
       0.48186967 0.48365027 0.48863488 0.49291074 0.5003274  0.49571738
       0.48988    0.4884476  0.48621935 0.48052177 0.48092973 0.47761825
       0.47976688 0.4759824  0.4810428  0.48358053 0.4896995  0.48775607
       0.48845947 0.48163626 0.4783075  0.47613925 0.47524858 0.478986
       0.4785511  0.4836624  0.47954634 0.4776491  0.48160934 0.47846857
       0.4759387  0.4734334  0.4753505  0.4768714  0.4813893  0.48451617
       0.4809152  0.48028225 0.48412064 0.48871896 0.49092242 0.48793992
       0.48604706 0.4847601  0.48148325 0.47716945 0.47934493 0.4815559
       0.48570117 0.476896   0.4769726  0.4790911  0.47814733 0.47847372
       0.47762838 0.47052544 0.4676497  0.4624416  0.46238935 0.46225113
       0.45584628 0.45175663 0.45171845 0.4562689  0.46202287 0.45723644
       0.4559435  0.45677936 0.46294767 0.47124648 0.47225267 0.47096685
       0.4658175  0.46110532 0.46422777 0.46248558 0.45522165 0.45167378
       0.45295504 0.4552273  0.44807217 0.4425756  0.44239047 0.444262
       0.44206715 0.44537133 0.44586852 0.44561613 0.43717283 0.44003224
       0.44366306 0.44380724 0.45074037 0.4536921  0.45256495 0.4520807
       0.45112494 0.45399073 0.45519212 0.46008974 0.46197698 0.45987296
       0.46237183 0.4647772  0.46286833 0.46495423 0.4586872  0.45635626
       0.45353281 0.45148867 0.4554184  0.45717728 0.45708904 0.45801127
       0.45670196 0.45556748 0.45303103 0.45066854 0.4552094  0.45512974
       0.45936364 0.46255708 0.46434605 0.46567768 0.46369436 0.45903355
       0.45711875 0.45360643 0.44967028 0.44329578 0.4481724  0.4454219
       0.4457831  0.4513038  0.4568139  0.4586644  0.4600454  0.454515
       0.4493416  0.44858563 0.44477877 0.44289824 0.44665512 0.45119953
       0.45109093 0.44904613 0.44505057 0.44585836 0.4454957  0.44541788
       0.44921857 0.44641122 0.44254017 0.43965834 0.44221207 0.44256127
       0.43601787 0.44071332 0.4459395  0.45212355 0.4554484  0.45408344
       0.45244938 0.44631252 0.4455498  0.4460687  0.44216818 0.4456364
       0.45096865 0.4537077  0.46075717 0.45340276 0.44249555 0.44301027
       0.45009586 0.44925272 0.43764532 0.43258256 0.43909183 0.44268596
       0.4472886  0.44692147 0.44959822 0.4501078  0.45300162 0.4492007
       0.44937035 0.4504997  0.45401528 0.4556772  0.45451713 0.45159253
       0.45193687 0.46053657 0.462726   0.46090898 0.45062447 0.44500482
       0.441107   0.44083497 0.43759263 0.4295081  0.4315562  0.4317832
       0.43490785 0.4353252  0.44056877 0.4371395  0.4380041  0.44062445
       0.44112325 0.44320524 0.4420255  0.44115487 0.44635415 0.44868794
       0.45324636 0.45957565 0.460918   0.46113592 0.45730126 0.45323786
       0.45482796 0.4563296  0.46081048 0.46130338 0.46773976 0.4572119
       0.45943835 0.45633966 0.45496476 0.44469935 0.44586053 0.44305733
       0.43936154 0.43972936 0.4407259  0.43773583 0.43945938 0.44193456
       0.4343791  0.4334101  0.43474647 0.4289421  0.43413523 0.4378296
       0.44164452 0.44237834 0.45085883 0.4425798  0.4403471  0.43633887
       0.44151253 0.44637334 0.44853896 0.44855985 0.45173734 0.44379863
       0.44288167 0.44451886 0.4408744  0.4423818  0.4477361  0.45148796
       0.44786483 0.4491139  0.45080054 0.4488766  0.44284558 0.44423977
       0.44966236 0.45541626 0.45433655 0.45114622 0.45306674 0.4417422
       0.43699288 0.43703148 0.43745133 0.4409853  0.44300348 0.455416
       0.45621035 0.45847043 0.45974505 0.4595796  0.4567098  0.45247775
       0.44836363 0.44892827 0.4451489  0.44984803 0.44766933 0.44317135
       0.4440923  0.44251835 0.4478839  0.45633957 0.45817083 0.45569223
       0.45821914 0.4543626  0.45020562 0.44881344 0.45476097 0.45786452
       0.45709175 0.4558641  0.45213178 0.44403067 0.44571713 0.4485699
       0.44686297 0.44674307 0.44514823 0.4476148  0.45009035 0.45037523
       0.4565123  0.45773292 0.4589985  0.4512731  0.4451256  0.4472318
       0.44847998 0.44384205 0.44474307 0.44716278 0.44925976 0.44057783
       0.44148415 0.44171774 0.436185   0.43513486 0.43893653 0.4443699
       0.4468345  0.44629446 0.4446364  0.44049454 0.43858925 0.43700576
       0.43330148 0.42884216 0.43547916 0.43583864 0.43790108 0.43262506
       0.4271077  0.42586398 0.42456162 0.41801772 0.42418334 0.4249077
       0.4251213  0.42808858 0.42970115 0.42819843 0.4265243  0.42458087
       0.43195623 0.44079098 0.44464523 0.44747645 0.4444531  0.44435763
       0.44486773 0.44511074 0.44532567 0.44265598 0.43650013 0.43124896
       0.43454096 0.44030535 0.43669355 0.43714076 0.44118208 0.43580595
       0.437173   0.44049308 0.4450707  0.4512639  0.44522154 0.4404493
       0.4383054  0.4423804  0.44557884 0.4461577  0.44170105 0.44501743
       0.44143775 0.44567898 0.44151187 0.44148675 0.44604343 0.45550922
       0.45525682 0.452847   0.45016238 0.4504259  0.44976535 0.44537422
       0.4464413  0.44782564 0.4533405  0.4507486  0.4470588  0.43888065
       0.44164658 0.44392017 0.44351763 0.44563344 0.4419579  0.43213773
       0.42954126 0.42145067 0.42097452 0.42697522 0.42986608 0.43559894
       0.43356878 0.4290208  0.4257134  0.41970342 0.42509764 0.4320975
       0.43117303 0.43173146 0.43358663 0.43695793 0.43266577 0.43566662
       0.43696675 0.43827045 0.43899024 0.4352676  0.42282155 0.4172961
       0.41670358 0.4206306  0.4203084  0.42080107 0.41675422 0.4213492
       0.422321   0.42239305 0.42685804 0.41934946 0.41503084 0.41808304
       0.41229397 0.4141019  0.413553   0.41628435 0.42229506 0.42623478
       0.42435473 0.4242148  0.42445847 0.4233372  0.42394772 0.42925912
       0.43105784 0.42847854 0.42525128 0.42518905 0.42580432 0.42555672
       0.42304417 0.4209258  0.42066815 0.4222913  0.42417392 0.43278566
       0.4355686  0.43576446 0.43340623 0.43610168 0.44057885 0.44372594
       0.43797967 0.44093812 0.44181785 0.44279128 0.4442994  0.44173756
       0.44442412 0.45110813 0.4509509  0.45278212 0.45253745 0.45246357
       0.45053226 0.45776924 0.4579954  0.45747745 0.45392427 0.4506838
       0.45284486 0.45632562 0.46121308 0.4619404  0.46333    0.46196985
       0.46597138 0.46243846 0.46495262 0.46513978 0.4597811  0.45750138
       0.447751   0.4375196  0.4317857  0.42753133 0.42353764 0.424147
       0.42828074 0.42942455 0.4271434  0.41783336 0.4216944  0.42955148
       0.42708397 0.43145925 0.43270192 0.4316306  0.437926   0.44054672
       0.43584174 0.43444285 0.4386646  0.44006848 0.43720296 0.43169937
       0.4385493  0.44499224 0.4386437  0.43265316 0.43165678 0.43488517
       0.4355327  0.43785435 0.4374438  0.4371878  0.43729237 0.43524444
       0.42801318 0.42701083 0.42700744 0.42779842 0.43249258 0.43858066
       0.44401494 0.44963437 0.44485074 0.44732186 0.4510061  0.45158067
       0.44616213 0.4480326  0.44262958 0.44240883]]]


Function to plot the results:


```python
###################################################################
#                 Plot Result function
####################################################################
def get_plot_datas(ids_file,data_file):
    ids_file=pd.read_csv(ids_file)
    data_file=pd.read_csv(data_file)
    data_file=data_file[['indf','dnbr','ndvi']]
    datas=data_file[(data_file['indf'].isin(ids_file.indf))]
    return datas[['ndvi','dnbr']].to_numpy()
```

Let's have a brief look at the datas: 
nb: what we call the mean model is simply: $$y_{pred}=\bar y_{obs}$$ 


```python
#################Datas summary####################
datas=get_plot_datas(test_ids_file,data_file)
x=datas[:,0]
y=datas[:,1]
print("Observed statistics:")
print()
print("mean dnbr = "+str(np.mean(y)))
print("dnbr variance = "+str(np.square(np.std(y))))
print()
a,b=np.polyfit(x,y,1)
print("Results from the linear model:")
print()
print("a (supposed to be equivalent to w in the last section) = "+str(a))
print("b = "+str(b))
linear_mae=sum(abs(y-(a*x+b)))/len(y)
linear_mse=sum(np.square(y-(a*x+b)))/len(y)
print("linear_model_mae = "+str(linear_mae))
print("linear_model_mse = "+str(linear_mse))
print()
print("NN model results:")
print()
y_pred=model.predict(x)
print("mean dnbr predicted = "+str(np.mean(y_pred[:,0])))
print("mae_from_mean_model ="+str(np.sum(np.abs(y-np.mean(y)))/len(y)))
```

    Observed statistics:
    
    mean dnbr = 2.481034895608084
    dnbr variance = 0.7607728299290712
    
    Results from the linear model:
    
    a (supposed to be equivalent to w in the last section) = 3.001565491043912
    b = 0.9982055323841632
    linear_model_mae = 0.0803045100039715
    linear_model_mse = 0.009984939104477841
    
    NN model results:
    
    mean dnbr predicted = 0.8054833
    mae_from_mean_model =0.7521578471518485


Ready to plot:


```python
##############################PLOTS#######################################
abscisses=[0,1]
ordonnees=[0,1]
figure,axis = plt.subplots(3,2,figsize = (20, 20))
xy = np.vstack([x,y])
z = gaussian_kde(xy)(xy)
axis[0,0].scatter(x,y,c=z,s=1)
axis[0,0].plot(x,a*x+b,'-',color='black',label="linear model")
axis[0,0].plot(x,first_layer_weights[0][0]*x+first_layer_biases[0],c="red",label="NN model")
axis[0,0].set_title("DNBR vs NDVI: Both models")
axis[0,0].legend()
axis[0,1].plot(epochs, loss, "bo", label="Training MAE")
axis[0,1].plot(epochs, val_loss, "b", label="Validation MAE")
axis[0,1].set_title("Training and validation MAE")
axis[0,1].legend()
xy = np.vstack([y,y_pred[:,0]])
z = gaussian_kde(xy)(xy)
axis[1,0].scatter(y,y_pred[:,0],c=z,s=1)
axis[1,0].plot(abscisses,ordonnees,label="y=x")
axis[1,0].set_title("NN Prévisions vs observées")
axis[1,0].legend()
res=np.subtract(y_pred[:,0],y)
xy = np.vstack([x,res])
z = gaussian_kde(xy)(xy)
axis[1,1].scatter(x,res,c=z,s=1)
axis[1,1].set_title("Résidus vs NDVI")
axis[2,0].hist(y_pred)
axis[2,0].set_title("Predictions distribution")
axis[2,1].hist(y)
axis[2,1].set_title("dnbr distribution")
plt.show()
```


    
![png](output_15_0.png)
    


Clearly, our network invented the mean!

In the case of the linear model, the mse being:$$f(a,b)=\frac{1}{n}\Sigma(ax_{obs}+b-y_{obs})²$$ the gradient coordinates are: $$\frac{\partial f}{\partial a}=\frac{2}{n}\Sigma x_{obs}(ax_{obs}+b-y_{obs})$$ and $$\frac{\partial f}{\partial b}=\frac{2}{n}\Sigma (ax_{obs}+b-y_{obs})$$ the sum being taken on the batch data.

The minimun is thus obtained solving $$\frac{\partial f}{\partial a}=0$$ and 
$$\frac{\partial f}{\partial b}=0$$ which leads to: $$a=\frac{cov(x,y)}{s²_{x}}$$ and $$b=\bar{y}-a\bar{x}$$

If, by any chance the gradient descent gets to $a\approx 0$ then $b\approx \bar{y}$.
So the question here is why does our descent gets to $a\approx 0$? One only answer is possible: some batches of datas leads to independs x vs y.
