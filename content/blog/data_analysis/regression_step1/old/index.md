---
title: " Basic regression with keras"
page: true
date: 2022-04-15
weight: 1
math: true
categories: -remote sensing
summary: Simple regression vs simplest NN between simulated NDVI and  DNBR spectral indices variables. The second being actually a llinear transform of the former with a small gaussian noise added. The purpose being to test the algorithm before applying it to real remote sensed datasets.
---



# Linear regression with keras with simulated datas

In this serie we will be dealing with a simple csv data file which structure is:


|indf|ndvi|dnbr|
|----|---|---|
||||

Basically, these datas are simulated random ndvi and $dnbr=3*ndvi+1+\epsilon$
with $\epsilon\sim \mathcal{N}(0,0.1)$ and $ ndvi\sim \mathcal{U}[0,1]$

The few next lines are just a tip for final layout centering.


```python
from IPython.core.display import HTML as Center

Center(""" <style>
.output_png {
    display: table-cell;indf
    text-align: center;
    vertical-align: middle;
    height:1600;
    width:600;
    
}
</style> """)

```




 <style>
.output_png {
    display: table-cell;indf
    text-align: center;
    vertical-align: middle;
    height:1600;
    width:600;

}
</style> 



Let's begin by charging the necessary libraries:


```python
import os
import re
import random
from matplotlib import pyplot as plt
from numpy.polynomial.polynomial import polyfit
from scipy.stats import gaussian_kde
import pandas as pd
import numpy as np
from tensorflow import keras
import keras.backend as K
from tensorflow.keras import layers
from generator_from_one_file import DataGenerator
```

Then we can prepare the datas


```python
data_file = "test_data.csv"
training_ids_file="training.csv"
#training_ids_file=data_path+"train.csv"
test_ids_file="test.csv"
#test_ids_file=data_path+"extra.csv"
validation_ids_file="validation.csv"
#validation_ids_file=data_path+"val.csv"
```

Doing a large usage of generators (one can refer to generator_from_one_file.py):


```python
batch_size=128
training=DataGenerator(training_ids_file,data_file,batch_size)
training_count=training.get_infos()
test=DataGenerator(test_ids_file,data_file,batch_size)
test_count=training.get_infos()
validation=DataGenerator(validation_ids_file,data_file,batch_size)
validation_count=validation.get_infos()
```

Time now to implement the network


```python
def build_model():
    model = keras.Sequential([
        layers.Input(shape=(1,)),
        layers.Dense(1,activation="linear"),
        
    ])
    opt = keras.optimizers.Adam()
    model.compile(optimizer=opt, loss="mse", metrics=["mae"])
    return model
```


```python

```


```python
class GetWeights(keras.callbacks.Callback):
  def __init__(self):
    super(GetWeights, self).__init__()
    self.weight_dict = {}
  def on_epoch_end(self, epoch, logs=None):
    w = model.layers[0].get_weights()[0]
    b = model.layers[0].get_weights()[1]
    if epoch == 0:
          # create array to hold weights and biases
        self.weight_dict['w_'] = w
        self.weight_dict['b_'] = b
    else:
          # append new weights to previously-created weights array
          self.weight_dict['w_'] = np.dstack(
              (self.weight_dict['w_'], w))
          # append new weights to previously-created weights array
          self.weight_dict['b_'] = np.dstack(
              (self.weight_dict['b_'], b))

```


```python
gw = GetWeights()
```

Let's define the Callback function that will allow us to analyse our model


```python
callbacks = [
    keras.callbacks.ModelCheckpoint('one_file.keras',
                                    save_best_only=True),
    keras.callbacks.TensorBoard(log_dir="./logs",histogram_freq=1),
    gw
]
```

Build and learn:


```python
model=build_model()
model.summary()
history = model.fit(training,
                    batch_size=batch_size,
                    steps_per_epoch=int(training_count/batch_size),
                    epochs=1000,
                    validation_data=validation,
                    validation_steps=int(validation_count/batch_size),
                    callbacks=callbacks,
                    use_multiprocessing=True,
                    workers=30,
                    )

```

    2022-04-22 00:04:08.981059: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
    To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
    2022-04-22 00:04:10.055655: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 4369 MB memory:  -> device: 0, name: Quadro P2000, pci bus id: 0000:3b:00.0, compute capability: 6.1
    2022-04-22 00:04:10.056393: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:1 with 4369 MB memory:  -> device: 1, name: Quadro P2000, pci bus id: 0000:af:00.0, compute capability: 6.1


    Model: "sequential"
    _________________________________________________________________
     Layer (type)                Output Shape              Param #   
    =================================================================
     dense (Dense)               (None, 1)                 2         
                                                                     
    =================================================================
    Total params: 2
    Trainable params: 2
    Non-trainable params: 0
    _________________________________________________________________
    Epoch 1/1000
     1/26 [>.............................] - ETA: 38s - loss: 21.3613 - mae: 3.8376WARNING:tensorflow:Callback method `on_train_batch_end` is slow compared to the batch time (batch time: 0.0028s vs `on_train_batch_end` time: 0.0037s). Check your callbacks.
    26/26 [==============================] - 4s 96ms/step - loss: 24.7462 - mae: 4.0529 - val_loss: 21.3078 - val_mae: 3.6848
    Epoch 2/1000
    26/26 [==============================] - 3s 96ms/step - loss: 18.2731 - mae: 3.3677 - val_loss: 19.5558 - val_mae: 3.5160
    Epoch 3/1000
    26/26 [==============================] - 4s 112ms/step - loss: 22.3376 - mae: 3.7837 - val_loss: 17.6091 - val_mae: 3.2963
    Epoch 4/1000
    26/26 [==============================] - 3s 98ms/step - loss: 18.7230 - mae: 3.4175 - val_loss: 19.0059 - val_mae: 3.4903
   
   
	--------
   
    Epoch 999/1000
    26/26 [==============================] - 4s 126ms/step - loss: 0.7266 - mae: 0.6583 - val_loss: 0.6853 - val_mae: 0.6335
    Epoch 1000/1000
    26/26 [==============================] - 4s 128ms/step - loss: 0.8341 - mae: 0.6691 - val_loss: 0.7582 - val_mae: 0.6499


Now, we load the infos and evaluate the net:


```python
##################Checking History############################
model = keras.models.load_model('one_file.keras')
print(f"Test MAE: {model.evaluate(test,batch_size=batch_size,steps=int(test_count/batch_size))[1]:.2f}")
loss = history.history["mae"]
val_loss = history.history["val_mae"]
epochs = range(1, len(loss) + 1)
```

    26/26 [==============================] - 0s 2ms/step - loss: 0.5691 - mae: 0.5600
    Test MAE: 0.56



```python
first_layer_weights = model.layers[0].get_weights()[0]
first_layer_biases  = model.layers[0].get_weights()[1]
print("w = "+str(first_layer_weights[0][0]))
print("b = "+str(first_layer_biases[0]))
print()
print("History of weight: \n"+str(gw.weight_dict['w_']))
print("History of bias: \n"+str(gw.weight_dict['b_']))
```

    w = 0.70854586
    b = 0.4554484
    
    History of weight: 
    [[[-1.48527575e+00 -1.46283615e+00 -1.43991995e+00 -1.41954553e+00
       -1.39996886e+00 -1.37924457e+00 -1.35924637e+00 -1.34033155e+00
       -1.31901765e+00 -1.29836893e+00 -1.27747130e+00 -1.25733423e+00
  
	-----
  
  
        6.94516122e-01  7.00218022e-01  7.07353413e-01  7.13163137e-01
        7.11501479e-01  7.13616192e-01  7.18799472e-01  7.20217168e-01
        7.15681195e-01  7.18091190e-01  7.14157224e-01  7.12311327e-01]]]
    History of bias: 
    [[[0.0258447  0.04986168 0.07449214 0.09769076 0.12025788 0.14377463
       0.16708355 0.18985195 0.2137599  0.23731452 0.26004398 0.28191623
       0.30426583 0.32790667 0.35146946 0.37262875 0.39337906 0.41602066
   
	-----
   
       0.44401494 0.44963437 0.44485074 0.44732186 0.4510061  0.45158067
       0.44616213 0.4480326  0.44262958 0.44240883]]]


Function to plot the results:


```python
###################################################################
#                 Plot Result function
####################################################################
def get_plot_datas(ids_file,data_file):
    ids_file=pd.read_csv(ids_file)
    data_file=pd.read_csv(data_file)
    data_file=data_file[['indf','dnbr','ndvi']]
    datas=data_file[(data_file['indf'].isin(ids_file.indf))]
    return datas[['ndvi','dnbr']].to_numpy()
```

Let's have a brief look at the datas: 
nb: what we call the mean model is simply: $$y_{pred}=\bar y_{obs}$$ 


```python
#################Datas summary####################
datas=get_plot_datas(test_ids_file,data_file)
x=datas[:,0]
y=datas[:,1]
print("Observed statistics:")
print()
print("mean dnbr = "+str(np.mean(y)))
print("dnbr variance = "+str(np.square(np.std(y))))
print()
a,b=np.polyfit(x,y,1)
print("Results from the linear model:")
print()
print("a (supposed to be equivalent to w in the last section) = "+str(a))
print("b = "+str(b))
linear_mae=sum(abs(y-(a*x+b)))/len(y)
linear_mse=sum(np.square(y-(a*x+b)))/len(y)
print("linear_model_mae = "+str(linear_mae))
print("linear_model_mse = "+str(linear_mse))
print()
print("NN model results:")
print()
y_pred=model.predict(x)
print("mean dnbr predicted = "+str(np.mean(y_pred[:,0])))
print("mae_from_mean_model ="+str(np.sum(np.abs(y-np.mean(y)))/len(y)))
```

    Observed statistics:
    
    mean dnbr = 2.481034895608084
    dnbr variance = 0.7607728299290712
    
    Results from the linear model:
    
    a (supposed to be equivalent to w in the last section) = 3.001565491043912
    b = 0.9982055323841632
    linear_model_mae = 0.0803045100039715
    linear_model_mse = 0.009984939104477841
    
    NN model results:
    
    mean dnbr predicted = 0.8054833
    mae_from_mean_model =0.7521578471518485


Ready to plot:


```python
##############################PLOTS#######################################
abscisses=[0,1]
ordonnees=[0,1]
figure,axis = plt.subplots(3,2,figsize = (20, 20))
xy = np.vstack([x,y])
z = gaussian_kde(xy)(xy)
axis[0,0].scatter(x,y,c=z,s=1)
axis[0,0].plot(x,a*x+b,'-',color='black',label="linear model")
axis[0,0].plot(x,first_layer_weights[0][0]*x+first_layer_biases[0],c="red",label="NN model")
axis[0,0].set_title("DNBR vs NDVI: Both models")
axis[0,0].legend()
axis[0,1].plot(epochs, loss, "bo", label="Training MAE")
axis[0,1].plot(epochs, val_loss, "b", label="Validation MAE")
axis[0,1].set_title("Training and validation MAE")
axis[0,1].legend()
xy = np.vstack([y,y_pred[:,0]])
z = gaussian_kde(xy)(xy)
axis[1,0].scatter(y,y_pred[:,0],c=z,s=1)
axis[1,0].plot(abscisses,ordonnees,label="y=x")
axis[1,0].set_title("NN Prévisions vs observées")
axis[1,0].legend()
res=np.subtract(y_pred[:,0],y)
xy = np.vstack([x,res])
z = gaussian_kde(xy)(xy)
axis[1,1].scatter(x,res,c=z,s=1)
axis[1,1].set_title("Résidus vs NDVI")
axis[2,0].hist(y_pred)
axis[2,0].set_title("Predictions distribution")
axis[2,1].hist(y)
axis[2,1].set_title("dnbr distribution")
plt.show()
```


    
![png](output_15_0.png)
    


Clearly, our network invented the mean!

In the case of the linear model, the mse being:$$f(a,b)=\frac{1}{n}\Sigma(ax_{obs}+b-y_{obs})²$$ the gradient coordinates are: $$\frac{\partial f}{\partial a}=\frac{2}{n}\Sigma x_{obs}(ax_{obs}+b-y_{obs})$$ and $$\frac{\partial f}{\partial b}=\frac{2}{n}\Sigma (ax_{obs}+b-y_{obs})$$ the sum being taken on the batch data.

The minimun is thus obtained solving $$\frac{\partial f}{\partial a}=0$$ and 
$$\frac{\partial f}{\partial b}=0$$ which leads to: $$a=\frac{cov(x,y)}{s²_{x}}$$ and $$b=\bar{y}-a\bar{x}$$

If, by any chance the gradient descent gets to $a\approx 0$ then $b\approx \bar{y}$.
So the question here is why does our descent gets to $a\approx 0$? One only answer is possible: some batches of datas leads to independs x vs y.
