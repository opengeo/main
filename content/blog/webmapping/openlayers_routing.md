---
title: "Routing with OpenLayers, PostGIS and MapServer"
page: true
summary: This doc. presents a proof of concept orchestrating those three major software
categories: 
 webmapping
---

{{< embed-pdf url="blog/webmapping/itineraire_openlayers.pdf" >}}
