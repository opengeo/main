---
title: Associate Professor and Freelance geospatial developper
page: true
math: true
menu:
 main:
  identifier: about
  name: About
  url: /about
  weight: 300
---
{{< rawhtml >}}
	<div class= "clearfix::before" style="background-image:url(/img/logo_unamed.svg);opacity: 0.1;position: absolute;top: 0px;right: 0px;bottom: 0px;left: 10%;width: 40%;height:100%"></div>
{{< /rawhtml >}}

Associate professor  in Mathematics at Université Paul Sabatier, Toulouse, I mainly dedicated myself to agronomical or natural environment data analysis. Professionnal remote pilot, I use drone images in photogrammetry.

Involved in a geomatics bachelor, I am a PostGIS, OpenLayers or MapServer specialist, that I deploy in webmapping architectures:  [Géomatique, WebMapping en OpenSource, Ellipses, 2019](https://www.editions-ellipses.fr/accueil/2888-geomatique-webmapping-en-open-source-architectures-web-avec-postgis-openlayers-et-mapserver-9782340029682.html).

Manager of a technical team, Head of Department, Program director, I acquired good expertise in human ressources management.
 
My students being specialized in agronomy I had the chance to analyse with them datas like UAVS images of experimental fields.
To enhance productivity in that field, I am currently developping [OpenSource tools](https://gitlab.com/opengeo/young-crops-counting).

I also had the chance in the past years to collaborate  on data science  research projects, leading to some interesting publications like: [Combined Use of Multi-Temporal
Landsat-8 and Sentinel-2 Images for Wheat Yield Estimates at the Intra-Plot Spatial Scale, Agronomy, 2020.](https://www.mdpi.com/2073-4395/10/3/327)

Now I want to get more involved in the OpenSource community dealing with  Spatial data.
