---
title: "More Services"
page: true
---

##  FreeLance developer

>  Data science: Inferential statistics, Bayesian modeling
>  Keras, scikit-learn

##  Data Management


>  SQL, Numpy, Pandas..


##   Staff secondment

> Need a hand for a specific task -> **Contact me**

##  Expertise in GIS & Data strategies

> Think twice before messing! 

> Let's share **20 years** of experience.


**For further information, please contact me.**
